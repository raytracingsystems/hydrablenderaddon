# Last change: 15.11.2022
# Author: Ray Tracing Systems.
# Rescale and enter an object in bounding box [-1, 1]

import bpy
from mathutils import Vector
import numpy as np

# clear the console
from os import system
cls = lambda: system('cls')
cls() 
 
bpy.ops.object.transform_apply(location=True, rotation=True, scale=True) 
bpy.context.scene.tool_settings.transform_pivot_point = 'BOUNDING_BOX_CENTER'
bpy.ops.object.origin_set(type='GEOMETRY_ORIGIN', center='BOUNDS')

obj        = bpy.context.object
dim        = obj.dimensions
array      = np.array([dim[0], dim[1], dim[2]])
max_dim    = np.max(array)
mult_scale = 2.0 / max_dim

print("dim        = {}".format(dim))
print("max_dim    = {}".format(max_dim)) 
print("mult_scale = {}".format(mult_scale))

obj.scale *= mult_scale
obj.location = [0,0,0]
bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)

