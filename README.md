# The Hydra renderer plugin for Blender v4.0 (under development).

The plugin is at an early stage of development, so you should not expect miracles from it. 😊

The list of available features is [here.](http://www.ray-tracing.com/HydraRenderHelp.github.io/features.html)

## Installation

For the visualizer to work, you need to install the **engine** and the **plugin/add-on**.

### Installing the engine.

Currently, the engine is distributed together with a plugin for 3ds Max for Windows. Installation for Linux is under development. In the future, we will create an additional engine/blender installation for both operating systems. In the meantime, please use [this instruction.](https://github.com/Ray-Tracing-Systems/Hydra3dsMaxBinary)

### Installation in a Blender.

1. Copy the project to the Blender folder “scripts\\addons”. For example: “C:\\Program Files\\Blender\\2.90\\scripts\\addons\\”.
2. Start the blender.
3. In the **Edit — Preferences — Addons — Community** menu, enable **Hydra renderer**.
4. The renderer is ready to work.

### Tests (for developers and enthusiasts)

https://gitflic.ru/project/ray-tracing-systems/hydrablenderaddon_tests
