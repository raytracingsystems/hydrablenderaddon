# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
# (c) Ray Tracing Systems. 2014-2023. Copyright.


import bpy
from . import engine
from . import ui
from . import properties
from . import presets
# from . import version_update
from bpy.utils import register_class
import atexit


bl_info = {
    "name"       : "Hydra Renderer",
    "author"     : "Ray Tracing Systems",
    "blender"    : (4, 0, 0),
    "description": "Hydra renderer for Blender",
    "version"    : (0, 3, 0),
    "warning"    : "work in progress",
    "doc_url"    : "http://www.raytracing.ru/",
    "tracker_url": "https://vk.com/hydrarenderer",
    "support"    : 'COMMUNITY',
    "category"   : "Render"}



class HydraRenderer(bpy.types.RenderEngine):
    '''Main render class.'''
    bl_idname                   = 'HYDRA RENDERER'
    bl_label                    = "Hydra renderer"
    bl_use_eevee_viewport       = True
    bl_use_preview              = False
    bl_use_save_buffers         = False
    bl_use_exclude_layers       = True
    bl_use_texture_preview      = True
    bl_use_shading_nodes_custom = False

    def __init__(self):
        self.render_ref     = 0
        self.scenepath      = "C:/[Hydra]/pluginFiles/scenelib"
        self.render_w       = 100
        self.render_h       = 100
        self.img_size       = 10000
        self.debug          = True
        self.version_engine = "HYDRA ENGINE 2"

    def __del__(self):
        engine.free(self)


    def update(self, data, depsgraph):
        '''Export scene data for render.'''
        if self.is_preview:
            cscene  = bpy.context.scene.hydra_render
            use_osl = cscene.shading_system
            engine.update(self, data, depsgraph, preview_osl=use_osl)
        else:
            engine.update(self, data, depsgraph)


    def render(self, depsgraph):
        '''Render scene into an image.'''
        engine.render(self)



    def view_update(self, context, depsgraph):
        '''Update on data changes for viewport render.'''
        pass
        # engine.update(self, context.blend_data, depsgraph, context.region, context.space_data, context.region_data)
        # engine.reset(self, context.blend_data, depsgraph)
        # engine.sync(self, depsgraph, context.blend_data)


    def view_draw(self, context, depsgraph):
        '''Draw viewport render.'''
        pass


    def update_script_node(self, node):
        '''Compile shader script node.'''
        pass
        # if engine.with_osl():
        #     from . import osl
        #     osl.update_script_node(node, self.report)
        # else:
        #     self.report({'ERROR'}, "OSL support disabled in this build.")


    def update_render_passes(self, scene, srl):
        '''Update the render passes that will be generated.'''
        engine.register_passes(self, scene, srl)


def engine_exit():
    pass
    # engine.exit()


classes = (
    HydraRenderer,
)



def register():
    # Make sure we only registered the callback once.
    atexit.unregister(engine_exit)
    atexit.register(engine_exit)

    # engine.init()

    properties.register()
    ui.register()
    presets.register()

    for cls in classes:
        register_class(cls)

    # bpy.app.handlers.version_update.append(version_update.do_versions)


def unregister():
    from bpy.utils import unregister_class
    from . import ui
    from . import properties
    from . import presets
    import atexit

    # bpy.app.handlers.version_update.remove(version_update.do_versions)

    ui.unregister()
    properties.unregister()
    presets.unregister()

    for cls in reversed(classes):
        unregister_class(cls)
