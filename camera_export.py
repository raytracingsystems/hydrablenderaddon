import math
import mathutils
from . import utilities as util
from . import hydraPy as hy



def get_worldscale(scene, as_scalematrix=True):
    ws = 1

    scn_us = scene.unit_settings

    if scn_us.system in {'METRIC', 'IMPERIAL'}:
        ws = scn_us.scale_length

    if as_scalematrix:
        return mathutils.Matrix.Scale(ws, 4)
    else:
        return ws


def look_at(scene, matrix, dof_focus_distance):
    '''
    get pos, look_at and up vectors from matrix
    '''
    mat_rot  = mathutils.Matrix.Rotation(math.radians(-90.0), 4, 'X')

    m_matrix = mathutils.Matrix()
    ws       = mathutils.Matrix.Scale(scene.unit_settings.scale_length, 4)
    m_matrix = mat_rot @ matrix @ ws

    ws_scalar       = get_worldscale(scene, as_scalematrix=False)
    m_matrix[0][3] *= ws_scalar
    m_matrix[1][3] *= ws_scalar
    m_matrix[2][3] *= ws_scalar

    # transpose to extract columns
    m_matrix = m_matrix.transposed()

    pos      = m_matrix[3]
    forwards = -m_matrix[2]
    target   = (pos + dof_focus_distance*forwards)
    up       = m_matrix[1]

    return pos, target, up



def export_camera(scene):
    camera  = scene.camera
    cam_ref = hy.hrCameraCreate("blender camera")

    cam_matrix = camera.matrix_world.copy()
    pos, lookat, up = look_at(scene, cam_matrix, camera.data.hydra_render.dof_focus_distance)

    # cam_instance.motion = [(0.0, cam_matrix)]
    res_width  = scene.render.resolution_x
    res_height = scene.render.resolution_y
    aspect     = res_height / res_width

    # hfov = 2.0 * math.atan((0.5 * w) / (0.5 * h / math.tan(vfov * 0.5)))
    # vfov = 2.0 * math.atan((0.5 * h) / (0.5 * w / math.tan(hfov * 0.5)))

    if res_width == res_height:
        fov = math.degrees(camera.data.angle)
    else:
        fov = math.degrees(camera.data.angle * aspect)
        match camera.data.sensor_fit:
            case 'AUTO':
                pass
            case 'HORIZONTAL':
                pass
            case 'VERTICAL': # best match with Blender
                fov = 2.0 * math.degrees(math.atan(0.5 * camera.data.sensor_height / camera.data.lens))


    hy.hrCameraOpen(cam_ref, hy.HR_WRITE_DISCARD)
    cam_node = hy.hrCameraParamNode(cam_ref)
    cam_node.force_child("fov").text().set(util.round_to_string(fov, 4))
    cam_node.force_child("nearClipPlane").text().set(util.round_to_string(camera.data.clip_start, 4))
    cam_node.force_child("farClipPlane").text().set(util.round_to_string(camera.data.clip_end, 4))
    cam_node.force_child("up").text().set(util.array_round_to_string(up, 6))
    cam_node.force_child("position").text().set(util.array_round_to_string(pos, 6))
    cam_node.force_child("look_at").text().set(util.array_round_to_string(lookat, 6))
    cam_node.force_child("enable_dof").text().set(int(camera.data.hydra_render.enable_dof))
    cam_node.force_child("dof_lens_radius").text().set(str(camera.data.hydra_render.dof_lens_radius))

    cam_node.force_child("tiltShiftX").text().set(str(camera.data.shift_x)) # not work
    cam_node.force_child("tiltShiftY").text().set(str(camera.data.shift_y)) # not work

    hy.hrCameraClose(cam_ref)

    return cam_ref
