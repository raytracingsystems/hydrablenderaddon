import bpy
from . import utilities as util
from . import hydra_export
from . import hydraPy as hy
import numpy as np
import time
import cProfile
import pstats


def update(engine, data, depsgraph, region=None, v3d=None, rv3d=None, preview_osl=False):
    util.report(engine, "DEBUG", "update.")

    util.create_scenelib_folder(engine)

    # global_scale = engine.global_scale
    # if depsgraph.scene.unit_settings.system != 'NONE' and engine.use_scene_unit:
    #     global_scale *= depsgraph.scene.unit_settings.scale_length

    hydra_export.do_export(engine, depsgraph, True)



def reset(engine, data, depsgraph):
    util.report(engine, "DEBUG", "reset.")

    # prefs = bpy.context.preferences
    # if prefs.experimental.use_cycles_debug and prefs.view.show_developer_ui:
    #     _cycles.debug_flags_update(depsgraph.scene.as_pointer())
    # else:
    #     _cycles.debug_flags_reset()

    # data      = data.as_pointer()
    # depsgraph = depsgraph.as_pointer()
    # _cycles.reset(engine.session, data, depsgraph)



def sync(engine, depsgraph, data):
    util.report(engine, "DEBUG", "sync.")



def render(a_engine):
    # with cProfile.Profile() as profile:
    util.report(a_engine, "DEBUG", "--- Render ---")
    pixel_count = a_engine.img_size * 4
    img_data    = np.zeros(pixel_count, dtype=np.float32)

    start_time  = time.time()
    while True:
        time.sleep(0.5)
        info = hy.hrRenderHaveUpdate(a_engine.render_ref)
        a_engine.update_progress(info.progress * 0.01)

        if info.haveUpdateFB:
            util.copy_image_to_framebuffer(a_engine, img_data)

        if info.finalUpdate is True or a_engine.test_break():
            # image_path      = engine.scenepath + "/z_out.png"
            # hy.hrRenderSaveFrameBufferLDR(engine.render_ref, image_path)
            # util.report(engine, "DEBUG", "image saved at {}".format(image_path))
            break

    end_time     = time.time()
    elapsed_time = end_time - start_time
    message      = "Render time: " + util.round_to_string(elapsed_time, 2) + " sec."
    util.report(a_engine, "INFO", message)

    util.copy_alpha_to_framebuffer(a_engine, img_data)
    hy.hrRenderCommand(a_engine.render_ref, "exitnow", "")

    # results_prof = pstats.Stats(profile)
    # results_prof.sort_stats(pstats.SortKey.TIME)
    # # results_prof.print_stats()
    # results_prof.dump_stats("results.prof")
    # # # in terminal run: cprofilev -f results.prof




def free(engine):
    '''Delete any data for every shot'''
    print("Hydra renderer [DEBUG]:\tfree.")



def register_passes(engine, scene, view_layer):
    util.report(engine, "DEBUG", "register passes.")

    # Detect duplicate render pass names, first one wins.
    # listed = set()
    # for name, channelids, channeltype in list_render_passes(scene, view_layer):
    #     if name not in listed:
    #         engine.register_pass(scene, view_layer, name, len(channelids), channelids, channeltype)
    #         listed.add(name)
