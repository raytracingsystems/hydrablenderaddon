from . import hydraPy as hy
import array
import numpy as np
from . import utilities as util
from .material_export import convert_material
from collections import Counter

from .helpers import log, UnexportableObjectException

class ExportCache:
    def __init__(self, name='Cache'):
        self.name           = name
        self.cache_keys     = set()
        self.cache_items    = {}
        self.serial_counter = Counter()

    def clear(self):
        self.__init__(name=self.name)

    def serial(self, name):
        s = self.serial_counter[name]
        self.serial_counter[name] += 1

        return s

    def have(self, ck):
        return ck in self.cache_keys

    def add(self, ck, ci):
        self.cache_keys.add(ck)
        self.cache_items[ck] = ci

    def get(self, ck):
        if self.have(ck):
            return self.cache_items[ck]
        else:
            raise Exception('Item {} not found in {}!'.format(ck, self.name))


class MeshDefinition:

    def __init__(self, mesh_name: str, mesh_ref: hy.HRMeshRef, materials_dict: dict):
        self.name          = mesh_name
        self.mesh_ref      = mesh_ref
        self.material_dict = materials_dict

class GeometryTracker:

    KnownExportedObjects = set()
    KnownModifiedObjects = set()
    NewExportedObjects = set()

    def __init__(self, engine, depsgraph):
        self.depsgraph  = depsgraph
        self.curr_scene = depsgraph.scene
        self.engine     = engine

        self.ExportedMeshes = ExportCache('ExportedMeshes')
        self.ExportedObjects = ExportCache('ExportedObjects')

        GeometryTracker.NewExportedObjects = set()
        self.objects_used_as_duplis = set()

    def export_mesh(self, obj, seq=0.0):
        obj_cache_key = (self.curr_scene, obj)

        if self.ExportedObjects.have(obj_cache_key):
            log("Exported Objects already contain {}".format(obj))
            return self.ExportedObjects.get(obj_cache_key)

        mesh_definitions = self.export_to_hydra(obj, seq=seq)
        self.ExportedObjects.add(obj_cache_key, mesh_definitions)

        return mesh_definitions

    def export_to_hydra(self, obj, seq=0.0):
        base_frame = self.curr_scene.frame_current

        try:
            mesh = obj.to_mesh()

            if mesh is None:
                raise UnexportableObjectException('Cannot export mesh from: {}'.format(obj.name))

            mesh_cache_key = (self.curr_scene, obj.data, seq)

            if not obj.is_deform_modified(self.curr_scene, 'RENDER') and self.ExportedMeshes.have(mesh_cache_key):
                return self.ExportedMeshes.get(mesh_cache_key)

            # mesh.calc_normals() deleted in v4.0
            mesh.calc_loop_triangles()
            mesh.calc_normals_split()

            uv_layer = None
            for layer in mesh.uv_layers:  # take first uv layer since hydra currently doesn't support multiple layers
                if layer:
                    uv_layer = layer
                    break

            # Export data
            points  = array.array('f', [])
            normals = array.array('f', [])
            uvs     = array.array('f', [])

            face_vert_indices = array.array('I', [])
            mat_indices       = array.array('I', [])

            unique_materials = dict()

            # Caches
            vert_vnorm_indices = {}  # mapping of vert index to exported vert index for verts with vert normals
            vert_use_vnorm     = set()   # Set of vert indices that use vert normals

            vert_index         = 0  # exported vert index

            # material_indices = list([p.material_index for p in mesh.polygons])
            util.report(self.engine, "DEBUG", f"{obj.name} mesh.materials = {[x for x in mesh.materials]}")

            hydra_mat_id_list = [] # list of hydra material ids
            remap_list = []
            for mesh_mat_id, material in enumerate(mesh.materials):
                hydra_mat_id = convert_material(self.engine, material, obj.name)
                hydra_mat_id_list.append(hydra_mat_id.id)
                remap_list.append(mesh_mat_id)                  
                remap_list.append(hydra_mat_id.id)
            
            util.report(self.engine, "DEBUG", f"hydra_mat_id_list: {hydra_mat_id_list}")
            util.report(self.engine, "DEBUG", f"remap_list: {remap_list}")

            # Iterating over all polygons of the mesh
            for poly in mesh.polygons:
                face_vert_idx = []

                # Mapping of material indices
                if hydra_mat_id_list:
                    mat_indices.append(hydra_mat_id_list[poly.material_index])

                # Recording unique materials
                if mesh.materials and poly.material_index < len(mesh.materials) and mesh.materials[poly.material_index] is not None:
                    unique_materials[mesh.materials[poly.material_index].name_full] = poly.material_index

                # Iterating over all loops of the polygon
                for loop_index in poly.loop_indices:
                    loop   = mesh.loops[loop_index]
                    vertex = mesh.vertices[loop.vertex_index]

                    # Getting UV coordinates, if they exist
                    if uv_layer:
                        uv_coord = uv_layer.data[loop_index].uv

                    # Determining whether to use smoothing for the vertex
                    if poly.use_smooth:
                        # Smoothed normals and UV coordinates
                        if uv_layer:
                            vert_data = (vertex.co[:], loop.normal[:], uv_coord[:])
                        else:
                            vert_data = (vertex.co[:], loop.normal[:])

                        # Checking the presence of the vertex in the dictionary
                        if vert_data not in vert_use_vnorm:
                            # Adding vertex data if it's not in the dictionary
                            points.extend(vert_data[0])
                            points.append(1.0)

                            normals.extend(vert_data[1])
                            normals.append(1.0)

                            if uv_layer:
                                uvs.extend(vert_data[2])

                            vert_vnorm_indices[vert_data] = vert_index
                            face_vert_idx.append(vert_index)
                            vert_index += 1
                        else:
                            # Using the existing vertex index
                            face_vert_idx.append(vert_vnorm_indices[vert_data])
                    else:
                        # Unsmoothed normals and UV coordinates
                        if uv_layer:
                            vert_data = (vertex.co[:], poly.normal[:], uv_coord[:])
                        else:
                            vert_data = (vertex.co[:], poly.normal[:])

                        points.extend(vert_data[0])
                        points.append(1.0)

                        normals.extend(vert_data[1])
                        normals.append(1.0)

                        if uv_layer:
                            uvs.extend(vert_data[2])

                        face_vert_idx.append(vert_index)
                        vert_index += 1

                # Adding vertex indices to the list
                face_vert_indices.extend(face_vert_idx[:3])

                # Handling quadrilaterals
                if len(face_vert_idx) > 3:
                    for i in range(2, len(face_vert_idx)):
                        face_vert_indices.extend((face_vert_idx[0], face_vert_idx[i - 1], face_vert_idx[i]))

            # Creating and exporting the mesh
            mesh_ref = hy.hrMeshCreate(obj.name)
            hy.hrMeshOpen(mesh_ref, hy.HR_TRIANGLE_IND3, hy.HR_WRITE_DISCARD)
            hy.hrMeshVertexAttribPointer4f(mesh_ref, "pos", points, 0)
            hy.hrMeshVertexAttribPointer4f(mesh_ref, "norm", normals, 0)
            if uv_layer:
                hy.hrMeshVertexAttribPointer2f(mesh_ref, "texcoord", uvs, 0)

            if len(hydra_mat_id_list) > 1:                                   
                mat_indices = np.array(mat_indices, dtype=np.int32)
                util.report(self.engine, "DEBUG", f"mat_indices: {mat_indices}")
                util.report(self.engine, "WARNING", f"Multi materials not implemented.")
                # Rendering stops because access to instance materials in HydraSceneExporter.instance_hydra_objects() is not implemented.
                # hy.hrMeshPrimitiveAttribPointer1iNumPy(mesh_ref, "mind", mat_indices, 0) 
                hy.hrMeshMaterialId(mesh_ref, mat_indices[0])
            else:
                hy.hrMeshMaterialId(mesh_ref, mat_indices[0])


            # util.report(self.engine, "DEBUG", f"mat_indices num.: {len(mat_indices)}")
            # util.report(self.engine, "DEBUG", f"triangles num.: {len(mesh.loop_triangles)}")
            hy.hrMeshAppendTriangles3(mesh_ref, len(face_vert_indices), face_vert_indices)
            hy.hrMeshClose(mesh_ref)

            mesh_definition = MeshDefinition(mesh_name=mesh.name,
                                            mesh_ref=mesh_ref,
                                            materials_dict=unique_materials)


            self.ExportedMeshes.add(mesh_cache_key, mesh_definition)

            #print("mesh_definition key - val : {} - {}".format(mesh_cache_key, mesh_definition))

            return mesh_definition

        except UnexportableObjectException as err:
            log('Object export failed, skipping object: {} {}'.format(mesh.name, err))
            return None
