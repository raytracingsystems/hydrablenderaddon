import math
from datetime import datetime
import time
import bpy
import mathutils


# Log msg to blender terminal
def log(msg):
    header = '[' + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + '][Hydra]: '
    print( header + '\t' + msg )

class UnexportableObjectException(Exception):
    #message = "Unexportable Object exception"
    pass

def is_light(obj):
    return obj.type == 'LIGHT'

def is_mesh(obj):
    return obj.type in {'MESH', 'CURVE', 'SURFACE', 'FONT'}


def get_all_mesh_objs(depsgraph: bpy.types.Depsgraph):
    ITERATED_OBJECT_TYPES = ('MESH', 'LIGHT')
    for obj in depsgraph.objects:
        if is_mesh(obj):
            yield obj.evaluated_get(depsgraph)


def get_obj_unique_id(in_obj, duplicator):
    if duplicator is not None:
        obj = in_obj.object
        persistent_id = ['%X' % i for i in in_obj.persistent_id if i < 0x7fffffff]
        unique_id = '%s_%s_dupli(%s)' % (duplicator.name, obj.name, 'x'.join(persistent_id))

    else:
        obj       = in_obj
        unique_id = in_obj.name

    return obj, unique_id


def get_matrix_to_hydra():
    return mathutils.Matrix.Rotation(math.radians(-90.0), 4, 'X')

class Instance:
    obj      = None
    mesh     = None
    light    = None
    matrices = []

    def __init__(self, obj, mesh=None, light=None, matrix=None):
        self.obj = obj

        if mesh is not None:
            self.mesh = mesh
        else:
            self.mesh = None

        if light is not None:
            self.light = light
        else:
            self.light = None

        if matrix is not None:
            mat = get_matrix_to_hydra() @ matrix
            self.matrices = [(0.0, mat)]
        else:
            self.matrices = []


    def append_matrix(self, matrix, seq, is_deform=False):
        sequence_len = len(self.matrices)

        mat = get_matrix_to_hydra() @ matrix

        if self.matrices:
            last_matrix = self.matrices[-1][1]
        else:
            last_matrix = None

        if not is_deform and (sequence_len > 1 and mat == last_matrix and last_matrix == self.matrices[-2][1]):
            self.matrices[-1] = (seq, mat)
        else:
            self.matrices.append((seq, mat))





# functions to test hydra api #####################################################################

def run_render_bg(render_ref, path):
    image_path = path + "/z_out.png"
    while True:
        time.sleep(0.5)
        info = hy.hrRenderHaveUpdate(render_ref)
        if info.finalUpdate is True:
            hy.hrRenderSaveFrameBufferLDR(render_ref, image_path)
            log("image saved at {}".format(image_path))
            hy.hrRenderCommand(render_ref, "exitnow", "")
            break


from .mesh_utils import *
from .light_math import *


def cornell_box(scenepath):
    init_info = hy.HRInitInfo()
    hy.hrSceneLibraryOpen(scenepath, hy.HR_WRITE_DISCARD, init_info)

    matW = hy.hrMaterialCreate("mWhite")
    hy.hrMaterialOpen(matW, hy.HR_WRITE_DISCARD)
    matNode = hy.hrMaterialParamNode(matW)
    diff = matNode.append_child("diffuse")
    diff.append_attribute("brdf_type").set_value("lambert")
    diff.append_child("color").text().set("0.5 0.5 0.5")
    hy.hrMaterialClose(matW)

    matMetallic = hy.hrMaterialCreate("metallic")
    hy.hrMaterialOpen(matMetallic, hy.HR_WRITE_DISCARD)
    matNode = hy.hrMaterialParamNode(matMetallic)
    diff = matNode.append_child("diffuse")
    diff.append_attribute("brdf_type").set_value("lambert")
    diff.append_child("color").text().set("0.207843 0.188235 0.0")
    refl = matNode.append_child("reflectivity")
    refl.append_attribute("brdf_type").set_value("phong")
    refl.append_child("color").text().set("0.367059 0.345882 0")
    refl.append_child("glossiness").text().set("0.5")
    refl.append_child("fresnel_IOR").text().set("14")
    refl.append_child("fresnel").text().set("1")
    hy.hrMaterialClose(matMetallic)

    matR = hy.hrMaterialCreate("mRed")
    hy.hrMaterialOpen(matR, hy.HR_WRITE_DISCARD)
    matNode = hy.hrMaterialParamNode(matR)
    diff = matNode.append_child("diffuse")
    diff.append_attribute("brdf_type").set_value("lambert")
    diff.append_child("color").text().set("0.5 0.0 0.0")
    hy.hrMaterialClose(matR)

    matG = hy.hrMaterialCreate("mGreen")
    hy.hrMaterialOpen(matG, hy.HR_WRITE_DISCARD)
    matNode = hy.hrMaterialParamNode(matG)
    diff = matNode.append_child("diffuse")
    diff.append_attribute("brdf_type").set_value("lambert")
    diff.append_child("color").text().set("0.0 0.5 0.0")
    hy.hrMaterialClose(matG)

    cubeOpenRef = createCornellCubeOpenRef("cornellBox", 4.0, matR.id, matG.id, matW.id)
    cubeRef = createCubeRef("cube", 0.5, matMetallic.id)

    camRef = hy.hrCameraCreate("my camera")
    hy.hrCameraOpen(camRef, hy.HR_WRITE_DISCARD)
    camNode = hy.hrCameraParamNode(camRef)
    camNode.append_child("fov").text().set("45")
    camNode.append_child("nearClipPlane").text().set("0.01")
    camNode.append_child("farClipPlane").text().set("100.0")
    camNode.append_child("up").text().set("0 1 0")
    camNode.append_child("position").text().set("0 0 15")
    camNode.append_child("look_at").text().set("0 0 0")
    hy.hrCameraClose(camRef)

    light = hy.hrLightCreate("my_area_light")
    hy.hrLightOpen(light, hy.HR_WRITE_DISCARD)
    lightNode = hy.hrLightParamNode(light)
    lightNode.attribute("type").set_value("area")
    lightNode.attribute("shape").set_value("rect")
    lightNode.attribute("distribution").set_value("diffuse")

    sizeNode = lightNode.append_child("size")
    sizeNode.append_attribute("half_length").set_value("1.0")
    sizeNode.append_attribute("half_width").set_value("1.0")

    intensityNode = lightNode.append_child("intensity")
    intensityNode.append_child("color").append_attribute("val").set_value("10 10 10")
    hy.hrLightClose(light)

    render_w = 512
    render_h = 512

    renderRef = hy.hrRenderCreate("HydraModern")
    hy.hrRenderEnableDevice(renderRef, 0, True)
    hy.hrRenderOpen(renderRef, hy.HR_WRITE_DISCARD)
    node = hy.hrRenderParamNode(renderRef)
    node.force_child("width").text().set(str(render_w))
    node.force_child("height").text().set(str(render_h))
    node.force_child("method_primary").text().set("pathtracing")
    node.force_child("method_caustic").text().set("pathtracing")
    node.force_child("trace_depth").text().set("5")
    node.force_child("diff_trace_depth").text().set("3")
    node.force_child("maxRaysPerPixel").text().set("512")
    hy.hrRenderClose(renderRef)

    scnRef = hy.hrSceneCreate("my scene")

    matrixT_1 = np.dot(translateM4x4(np.array([0.0, -2.555, 0.0])), scaleM4x4(np.array([3.65, 3.65, 3.65])))
    matrixT_2 = rotateYM4x4(DEG_TO_RAD * 180.0)
    matrixT_light = translateM4x4(np.array([0.0, 3.85, 0.0]))
    mI = identityM4x4()

    hy.hrSceneOpen(scnRef, hy.HR_WRITE_DISCARD)
    hy.hrMeshInstance(scnRef, cubeRef, matrixT_1.flatten())
    hy.hrMeshInstance(scnRef, cubeOpenRef, matrixT_2.flatten())
    hy.hrLightInstance(scnRef, light, matrixT_light.flatten())
    hy.hrSceneClose(scnRef)
    hy.hrFlush(scnRef, renderRef, camRef)

    run_render_bg(renderRef, scenepath)

    return
