import time
import numpy as np
import os
import ntpath
import bpy
import math
import mathutils

from . import hydraPy as hy
from . import helpers
from .geometry_export import GeometryTracker
from .camera_export import export_camera
from . import lamp_export
from . import material_export
from . import utilities as util


def do_export(engine, depsgraph, is_preview=False):
    try:
        scene = depsgraph.scene

        if scene is None:
            raise Exception('Scene is not valid for export')

        settings              = scene.hydra_render
        engine.version_engine = settings.version_engine

        util.report(engine, "DEBUG", "scenelib directory: " + engine.scenepath)
        start_time = time.time()
    #     hy.hello(engine.scenepath)
        # cornell_box(engine.scenepath)
        info = hy.HRInitInfo()
        # util.report(engine, "DEBUG", f"info.copyTexturesToLocalFolder = {info.copyTexturesToLocalFolder}")
        # util.report(engine, "DEBUG", f"info.localDataPath             = {info.localDataPath}")
        # util.report(engine, "DEBUG", f"info.sortMaterialIndices       = {info.sortMaterialIndices}")
        # util.report(engine, "DEBUG", f"info.computeMeshBBoxes         = {info.computeMeshBBoxes}")
        # util.report(engine, "DEBUG", f"info.saveChanges               = {info.saveChanges}")
        # util.report(engine, "DEBUG", f"info.vbSize                    = {info.vbSize}")

        #TODO: Fix this on windows (hydra api dir creation problem?)
        try:
            os.umask(0o022)
            os.mkdir(engine.scenepath)
            os.mkdir(engine.scenepath + "/data")
        except:
            pass

        hy.hrSceneLibraryOpen(engine.scenepath, hy.HR_WRITE_DISCARD, info)

        exporter = HydraSceneExporter(engine, depsgraph)
        scn_ref  = exporter.export_scene()

        # # export materials
        # current_time = time.time()
        # start_time = current_time
        # log("Exporting materials...")
        # export_materials(depsgraph)
        # log("Exported materials %.2f sec." % (time.time() - current_time))

        # # export geometry and lights
        # current_time = time.time()
        # log("Exporting geometry and lights...")
        # export_scene(depsgraph, is_preview)
        # log("Exported geometry and lights %.2f sec." % (time.time() - current_time))


        # self.scene_camera = Instance(self.scene.camera)
        util.report(engine, "DEBUG", "Exporting camera and render settings...")
        cam_ref              = export_camera(scene)

        match scene.camera.data.sensor_fit:
            case 'AUTO':
                util.report(engine, 'WARNING', 'Hydra supports only the vertical type of camera sensor. Other settings may have incomplete compatibility.')
            case 'HORIZONTAL':
                util.report(engine, 'WARNING', 'Hydra supports only the vertical type of camera sensor. Other settings may have incomplete compatibility.')
            case 'VERTICAL':
                pass

        engine.render_ref    = export_render_settings(engine, scene)

        # instances            = self.traverse_depsgraph()
        # material_map         = self.export_materials() TODO: for remap list

        hy.hrFlush(scn_ref, engine.render_ref, cam_ref)

        util.report(engine, "DEBUG", "Export done. Time total: %.2f sec." % (time.time() - start_time))

    except Exception as err:
        import traceback
        traceback.print_exc()

        return {'CANCELLED'}


def configure_mmlt_params(mmlt_estim_time):
    burn      = 64
    step_size = 0.25

    match mmlt_estim_time:
        case 0:
            burn = 256
            step_size = 0.25
        case 1:  # default
            burn = 1024
            step_size = 0.5
        case 2:
            burn = 4096
            step_size = 1.0
        case 3:
            burn = 16384
            step_size = 2.0
        case _:
            pass  # default case with no changes

    return burn, step_size


def export_render_settings(engine, scene):
    engine.render_w = int(scene.render.resolution_x * scene.render.resolution_percentage * 0.01)
    engine.render_h = int(scene.render.resolution_y * scene.render.resolution_percentage * 0.01)
    engine.img_size = engine.render_w * engine.render_h
    dev_id          = 0
    dev_enable      = True
    settings        = scene.hydra_render

    render_ref      = hy.hrRenderCreate("HydraModern")
    hy.hrRenderEnableDevice(render_ref, dev_id, dev_enable)
    hy.hrRenderOpen(render_ref, hy.HR_WRITE_DISCARD)

    node            = hy.hrRenderParamNode(render_ref)
    node.force_child("width").text().set(engine.render_w)
    node.force_child("height").text().set(engine.render_h)

    if settings.integrator_engine_2 == "PATH":
        node.force_child("method_primary").text().set("pathtracing")
        node.force_child("method_caustic").text().set("pathtracing")
    elif settings.integrator_engine_2 == "LT":
        node.force_child("method_primary").text().set("lighttracing")
        node.force_child("method_caustic").text().set("lighttracing")
    elif settings.integrator_engine_2 == "IBPT":
        node.force_child("method_primary").text().set("IBPT")
        node.force_child("method_caustic").text().set("IBPT")
    elif settings.integrator_engine_2 == "MMLT":
        node.force_child("method_primary").text().set("pathtracing")
        node.force_child("method_secondary").text().set("mmlt")        
        node.force_child("method_caustic").text().set("mmlt")                
        burn, step_size = configure_mmlt_params(settings.mmlt_estimate_time)        
        node.force_child("mmlt_burn_iters")    .text().set(burn)
        node.force_child("mmlt_step_power")    .text().set("normal")
        node.force_child("mmlt_step_size")     .text().set(step_size)
        node.force_child("mmlt_threads")       .text().set(524288)
        node.force_child("mmlt_multBrightness").text().set(1)
        node.force_child("mlt_med_enable")     .text().set(0)
        node.force_child("mlt_med_threshold")  .text().set(0.5)
    else:
        node.force_child("method_primary").text().set("pathtracing")
        node.force_child("method_caustic").text().set("pathtracing")


    if not settings.use_caustics: 
        node.force_child("method_caustic").text().set("none")
    
    node.force_child("trace_depth")     .text().set(settings.ray_bounces)
    node.force_child("diff_trace_depth").text().set(settings.diffuse_bounces)
    node.force_child("maxRaysPerPixel") .text().set(settings.samples)

    # QMC
    QMC_DOF_FLAG  = 1
    QMC_MTL_FLAG  = 2
    QMC_LGT_FLAG  = 4

    qmc_var = QMC_DOF_FLAG
    if settings.use_qmc_materials: qmc_var |= QMC_MTL_FLAG
    if settings.use_qmc_lights:    qmc_var |= QMC_LGT_FLAG
    node.force_child("qmc_variant").text().set(qmc_var)

    # Clamping
    bsdf_clamp  = settings.bsdf_clamp if settings.use_bsdf_clamp else 1000000

    node.force_child("envclamp")           .text().set(1000000)
    node.force_child("clamping")           .text().set(bsdf_clamp)

    # G-buffer
    node.force_child("evalgbuffer")        .text().set("1")

    if settings.auto_texture_resize: node.force_child("scenePrepass").text().set(1)
    else:                            node.force_child("scenePrepass").text().set(0)

    if settings.fast_background:     node.force_child("offline_pt")  .text().set(1)
    else:                            node.force_child("offline_pt")  .text().set(0)

    hy.hrRenderClose(render_ref)
    return render_ref



class HydraSceneExporter:

    geo_tracker      = None
    light_tracker    = None
    material_tracker = None
    material_map     = {}
    scn_ref          = None

    def __init__(self, engine, depsgraph):
        self.instances = {}
        self.scenepath = engine.scenepath
        self.directory = ntpath.dirname(engine.scenepath)
        self.depsgraph = depsgraph
        self.scene     = depsgraph.scene
        self.engine    = engine
        self.filepath  = ""


    def export_scene(self):
        current_time = time.time()
        util.report(self.engine, "DEBUG", "--- Scene objects export ---")

        # default gray material
        mat0_ref   = hy.hrMaterialCreate("gray")
        hy.hrMaterialOpen(mat0_ref, hy.HR_WRITE_DISCARD)
        material_export.add_diffuse_xml_node(self.engine, mat0_ref, [0.5,0.5,0.5], 0)
        hy.hrMaterialClose(mat0_ref)

        self.geo_tracker      = GeometryTracker(self.engine, self.depsgraph)
        self.light_tracker    = lamp_export.LampTracker(self.scene, self.engine)
        self.material_tracker = material_export.MaterialTracker(self.scene, self.filepath)

        # mesh_objs = get_all_mesh_objs(depsgraph)
        for obj_inst in self.depsgraph.object_instances:
            util.report(self.engine, "DEBUG", f'Analyzing object {obj_inst} : {obj_inst.object.type} - {obj_inst.object.name}')

            original_obj = obj_inst.object
            obj_matrix   = obj_inst.matrix_world.copy()

            if obj_inst.is_instance:  # Real dupli instance
                util.report(self.engine, "DEBUG", f"Instance object {obj_inst.object.name}")
                if not (obj_inst.hide or original_obj.hide_render or obj_inst.type == 'GROUP'):
                    self.export_object(original_obj, obj_inst, obj_matrix)

            else:  # Usual object
                if not original_obj.hide_render:
                    util.report(self.engine, "DEBUG", f"Export object {original_obj.name}")
                    self.export_object(original_obj, None, obj_matrix)
                else:
                    util.report(self.engine, "DEBUG", f"Object hidden: {original_obj.name}")

        util.report(self.engine, "DEBUG", "Scene objects export done in %.2f sec." % (time.time() - current_time))

        self.scn_ref = self.instance_hydra_objects()

        return self.scn_ref


    def instance_hydra_objects(self):
        util.report(self.engine, "DEBUG", "--- Instancing ---")

        self.scn_ref = hy.hrSceneCreate("blender scene")
        hy.hrSceneOpen(self.scn_ref, hy.HR_WRITE_DISCARD)

        #################################

        for id, instance in self.instances.items():

            matr = instance.matrices[0][1]

            if instance.mesh:
                util.report(self.engine, "DEBUG", f"Instancing mesh object: {instance.mesh.name}")

                matrix     = np.array(matr, dtype=np.float32)
                remap_list = self.create_remap_list(instance)

                hy.hrMeshInstanceRemap(self.scn_ref, instance.mesh.mesh_ref, matrix.flatten(),
                                       np.array(remap_list, dtype=np.int32), len(remap_list))

            elif instance.light:
                util.report(self.engine, "DEBUG", f"Instancing light object: {instance.light[0][1]}")

                mat_rot  = mathutils.Matrix.Rotation(math.radians(-90.0), 4, 'X')
                mat_rot2 = mathutils.Matrix.Rotation(math.radians(180.0), 4, 'Y')
                matr     = matr @ mat_rot2 @ mat_rot
                matrix   = np.array(matr, dtype=np.float32)
                hy.hrLightInstance(self.scn_ref, instance.light[0][0], matrix.flatten())

        self.light_tracker.export_world_environment(self.scn_ref)

        hy.hrSceneClose(self.scn_ref)
        return self.scn_ref
    

    def create_remap_list(self, a_instance):
        remap_list = []
        for mat_name, mat_id in a_instance.mesh.material_dict.items():
            # if len(self.material_map) < 1:
            #     util.report(self.engine, "WARNING", f"No material_map")
            #     continue

            hydra_mat_id = self.material_map.get(mat_name, None)
            util.report(self.engine, "DEBUG", f"Instancing mesh material [name, id]: [{mat_name}, {mat_id}]")

            if hydra_mat_id is not None:
                remap_list.append(mat_id)
                remap_list.append(hydra_mat_id)

            if len(remap_list) > 1:
                util.report(self.engine, "DEBUG", f"remap_list: {remap_list}")
            # else:                
            #     util.report(self.engine, "WARNING", f"No hydra_mat_id for remap list")

        return remap_list


    def export_object(self, in_obj, duplicator=None, matrix=None, seq=0.0):

        (obj, unique_id) = helpers.get_obj_unique_id(in_obj, duplicator)
        util.report(self.engine, "DEBUG", f"Object unique_id: {format(unique_id)}")

        if helpers.is_light(obj):
            if unique_id in self.instances:
                self.instances[unique_id].append_matrix(matrix, seq)
            else:
                self.instances[unique_id] = helpers.Instance(obj, light=self.light_tracker.export_light(obj, seq=seq), matrix=matrix)
            return

        if helpers.is_mesh(obj):
            if duplicator is not None:
                self.geo_tracker.objects_used_as_duplis.add(obj)

            if unique_id in self.instances:
                self.instances[unique_id].append_matrix(matrix, seq, is_deform=obj.is_deform_modified(self.scene, 'RENDER'))
            else:
                self.instances[unique_id] = helpers.Instance(obj, mesh=self.geo_tracker.export_mesh(obj, seq=seq), matrix=matrix)


    def export_materials(self, depsgraph):
            pass
