import bpy
from . import hydraPy as hy
from . import geometry_export
from . import material_export
import numpy as np
from . import utilities as util
import math
from . import texture_export as tex_exp


class LampTracker:
    # for partial mesh export
    KnownExportedObjects = set()
    KnownModifiedObjects = set()
    NewExportedObjects   = set()

    def __init__(self, scene, engine):
        self.curr_scene      = scene
        self.ExportedLamps   = geometry_export.ExportCache('ExportedLamps')
        self.ExportedObjects = geometry_export.ExportCache('ExportedObjects')
        self.ExportedFiles   = geometry_export.ExportCache('ExportedFiles')
        self.engine = engine

        # start fresh
        LampTracker.NewExportedObjects = set()
        self.objects_used_as_duplis    = set()


    def export_world_environment(self, scn_ref):
        envir_ref = hy.hrLightCreate("environment")

        hy.hrLightOpen(envir_ref, hy.HR_WRITE_DISCARD)

        light_node     = hy.hrLightParamNode(envir_ref)
        light_node.attribute("type").set_value("sky")
        intensity_node = light_node.append_child("intensity")
        color_node     = intensity_node.append_child("color")
        
        node_background = bpy.data.worlds["World"].node_tree.nodes["Background"]
        if node_background.inputs[0].is_linked:
            light_node.force_attribute("distribution").set_value("map")
            texture = node_background.inputs[0].links[0].from_node
            util.report(self.engine, "DEBUG", f"The texture of the environment: {texture}")
                                    
            # color_socket = material_export._socket(self.engine, node_background.inputs["Color"], "Background", "World", None)
            # tex_ref      = color_socket.get("tex_ref", hy.HRTextureNodeRef())
            # tex_info     = tex_exp.TextureInfo(tex_ref, 1, 1)
            
            # tex_info.bind_texture(color_node)

        else:                  
            light_node.force_attribute("distribution").set_value("uniform")
            color = node_background.inputs[0].default_value
            color_node.force_attribute("val").set_value(util.array_round_to_string(color, 4))
        

        multiplier = bpy.data.worlds["World"].node_tree.nodes["Background"].inputs[1].default_value
        intensity_node.append_child("multiplier").append_attribute("val").set_value(util.round_to_string(multiplier, 4))
        
        hy.hrLightClose(envir_ref)

        sky_matrix = np.array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1], dtype=np.float32)
        hy.hrLightInstance(scn_ref, envir_ref, sky_matrix.flatten())



    def build_and_export_light(self, obj, seq=0.0):
        obj_cache_key = (self.curr_scene, obj)

        if self.ExportedObjects.have(obj_cache_key):
            print("Exported Objects have ", obj)
            return self.ExportedObjects.get(obj_cache_key)

        lamp_definitions = self.export_light(obj, seq=seq)
        self.ExportedObjects.add(obj_cache_key, lamp_definitions)
        return lamp_definitions


    def export_light(self, obj, base_frame=None, seq=0.0):
        if base_frame is None:
            base_frame = self.curr_scene.frame_current

        try:
            lamp_definitions = []
            lamp = obj.data

            if lamp is None:
                raise geometry_export.UnexportableObjectException('Cannot export light obj')

            lamp_cache_key = (self.curr_scene, obj.data, seq)

            if self.ExportedLamps.have(lamp_cache_key):
                lamp_definitions.append(self.ExportedLamps.get(lamp_cache_key))
                return lamp_definitions

            lampRef   = hy.hrLightCreate(obj.name)
            hy.hrLightOpen(lampRef, hy.HR_WRITE_DISCARD)
            lightNode = hy.hrLightParamNode(lampRef)
            lightNode.attribute("type").set_value(lamp.hydra_render.hydra_light_type)

            if lamp.hydra_render.hydra_light_type in {'point', 'directional'}:
                lightNode.attribute("shape").set_value('point')
            else:
                lightNode.attribute("shape").set_value(lamp.hydra_render.area_shape)

            lightNode.attribute("distribution").set_value(lamp.hydra_render.light_distribution)

            if lamp.hydra_render.hydra_light_type == 'directional':
                lightNode.attribute("distribution").set_value('directional')
                lightNode.force_child("size").force_attribute("inner_radius").set_value(util.round_to_string(lamp.hydra_render.direct_inner_radius, 4))
                lightNode.child("size").force_attribute("outer_radius").set_value(util.round_to_string(lamp.hydra_render.direct_outer_radius, 4))
                # One thing works: either angular size, or shadow blur.
                # The angular size also changes the size of the source, it is more realistic and there is
                # support in the viewport, so we chose it.
                lightNode.force_child("angle_radius").force_attribute("val").set_value(util.round_to_string(math.degrees(lamp.hydra_render.direct_angle_radius * 0.5), 4))
                # lightNode.force_child("shadow_softness").force_attribute("val").set_value(lamp.hydra_render.direct_shadow_softness)
                lamp.hydra_render.multiplier *= math.pi
            elif lamp.hydra_render.hydra_light_type == 'area':
                if lamp.hydra_render.area_shape == 'rect':
                    lightNode.append_child("size").append_attribute("half_length").set_value(util.round_to_string(lamp.hydra_render.half_x, 4))
                    lightNode.child("size").append_attribute("half_width").set_value(util.round_to_string(lamp.hydra_render.half_y, 4))
                elif lamp.hydra_render.area_shape == 'cylinder':
                    lightNode.append_child("size").append_attribute("radius").set_value(util.round_to_string(lamp.hydra_render.radius, 4))
                    lightNode.child("size").append_attribute("height").set_value(util.round_to_string(lamp.hydra_render.height, 4))
                    lightNode.child("size").append_attribute("angle").set_value(util.round_to_string(lamp.hydra_render.angle, 4))
                elif lamp.hydra_render.area_shape in {'disk', 'sphere'}:
                    lightNode.append_child("size").append_attribute("radius").set_value(util.round_to_string(lamp.hydra_render.radius, 4))

            if lamp.hydra_render.light_distribution == 'ies':
                distribution = lightNode.append_child("ies")

                distribution.append_attribute("data").set_value(util.get_real_path(lamp.hydra_render.ies_path))
                #
                distribution.append_attribute("matrix")
                samplerMatrix = np.array([1, 0, 0, 0,
                                          0, 1, 0, 0,
                                          0, 0, 1, 0,
                                          0, 0, 0, 1], dtype=np.float32)
                hy.WriteMatrix4x4(distribution, "matrix", samplerMatrix.flatten())

            intensityNode = lightNode.append_child("intensity")
            intensityNode.append_child("color").append_attribute("val").set_value(util.array_round_to_string(lamp.hydra_render.color, 4))
            intensityNode.append_child("multiplier").append_attribute("val").set_value(util.round_to_string(lamp.hydra_render.multiplier, 4))

            if lamp.hydra_render.light_distribution == 'spot':
                spot_out = math.degrees(lamp.hydra_render.spot_angle)
                spot_in  = max(spot_out + lamp.hydra_render.spot_blend * (0.0 - spot_out), 0.0) * 0.99
                lightNode.append_child("falloff_angle").append_attribute("val").set_value(util.round_to_string(spot_out, 4))
                lightNode.append_child("falloff_angle2").append_attribute("val").set_value(util.round_to_string(spot_in, 4))

            hy.hrLightClose(lampRef)

            lamp_definition = (lampRef, lamp)
            self.ExportedLamps.add(lamp_cache_key, lamp_definition)
            lamp_definitions.append((lampRef, lamp_definition))
            return lamp_definitions

        except geometry_export.UnexportableObjectException as err:
            print("Object export failed, skipping this object: ", err)
            return None
