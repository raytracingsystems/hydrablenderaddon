import bpy
from . import hydraPy as hy
from . import geometry_export
import numpy as np
from . import utilities as util
from . import texture_export as tex_exp

MISSING_TEXTURE_COLOR = [1, 0, 1]

class MaterialExportException(Exception):
    #print("Material Export Exception")
    pass


class MaterialTracker:

    KnownExportedObjects = set()
    KnownModifiedObjects = set()
    NewExportedObjects   = set()

    def __init__(self, scene, filepath):
        self.curr_scene = scene
        self.filepath   = filepath

        self.ExportedMaterials = geometry_export.ExportCache('ExportedMaterials')
        # start fresh
        MaterialTracker.NewExportedObjects = set()


# return mat_id in Hydra
def convert_material(engine, material, obj_name=""):
    # print("Converting Cycles node tree of material", material.name_full)
    output = material.node_tree.get_output_node("CYCLES")
    if output is None:
        return 0

    link = util.get_link(output.inputs["Surface"])
    if link is None:
        return 0

    exported = _node(engine, link.from_node, link.from_socket, material, obj_name)

    return exported["mat_ref"]


def _socket(engine, socket, material, obj_name, group_node):
    link = util.get_link(socket)
    if link:
        return _node(engine, link.from_node, link.from_socket, material, obj_name, group_node)

    if not hasattr(socket, "default_value"):
        return {"value": 0, "tex_ref": hy.HRTextureNodeRef()}

    try:
        res = list(socket.default_value)[:3]
        # util.report(engine, "DEBUG", f"_socket: {material.name} : {res}")
        return {"value": res, "tex_ref": hy.HRTextureNodeRef()}
    except TypeError:
        return {"value": socket.default_value, "tex_ref": hy.HRTextureNodeRef()}


def _node(engine, node, output_socket, material, obj_name="", group_node_stack=None):
    # output dictionary can contain:
    # mat_ref : HRMaterialRef
    # tex_ref : HRTextureNodeRef
    # value : float or float3, etc.
    output_dict = {}
    
    ##################################################################

    if node.bl_idname == "ShaderNodeBsdfPrincipled":
        base_color_socket = _socket(engine, node.inputs["Base Color"], material, obj_name, group_node_stack)
        metallic_socket   = _socket(engine, node.inputs["Metallic"], material, obj_name, group_node_stack)
        refl_rough_socket = _socket(engine, node.inputs["Roughness"], material, obj_name, group_node_stack)
        ior_socket        = _socket(engine, node.inputs["IOR"], material, obj_name, group_node_stack)
        spec_ior_socket   = _socket(engine, node.inputs["Specular IOR Level"], material, obj_name, group_node_stack)        
        # anisotropic     = _socket(engine, node.inputs["Anisotropic"], material, obj_name, group_node_stack)
        # coat_weight     = _socket(engine, node.inputs["Coat Weight"], material, obj_name, group_node_stack)
        transmiss_socket  = _socket(engine, node.inputs["Transmission Weight"], material, obj_name, group_node_stack)
        alpha_socket      = _socket(engine, node.inputs["Alpha"], material, obj_name, group_node_stack)
        bump_socket       = _socket(engine, node.inputs["Normal"], material, obj_name, group_node_stack)
        emiss_val_socket  = _socket(engine, node.inputs["Emission Strength"], material, obj_name, group_node_stack)
        emiss_col_socket  = _socket(engine, node.inputs["Emission Color"], material, obj_name, group_node_stack)

        base_color        = base_color_socket.get("value", [1, 1, 1])
        metallic          = metallic_socket.get("value", 1)
        refl_rough        = refl_rough_socket.get("value", 0)
        ior               = ior_socket.get("value", 1.5)
        spec_ior          = spec_ior_socket.get("value", 0.5)
        transmiss         = util.clamp(transmiss_socket.get("value", 0), 0.0, 1.0)
        emiss_color       = emiss_col_socket.get("value", [1, 1, 1])
        emission          = emiss_val_socket.get("value", 0)

        base_tex_ref       = base_color_socket.get("tex_ref", hy.HRTextureNodeRef())
        metallic_tex_ref   = metallic_socket.get("tex_ref", hy.HRTextureNodeRef())
        refl_rough_tex_ref = refl_rough_socket.get("tex_ref", hy.HRTextureNodeRef())
        transmiss_tex_ref  = transmiss_socket.get("tex_ref", hy.HRTextureNodeRef())
        alpha_tex_ref      = alpha_socket.get("tex_ref", hy.HRTextureNodeRef())
        bump_tex_ref       = bump_socket.get("tex_ref", hy.HRTextureNodeRef())
        emiss_col_tex_ref  = emiss_col_socket.get("tex_ref", hy.HRTextureNodeRef())

        base_tex_info       = tex_exp.TextureInfo(base_tex_ref, 1, 1)
        metallic_tex_info   = tex_exp.TextureInfo(metallic_tex_ref, 1, 1)
        refl_rough_tex_info = tex_exp.TextureInfo(refl_rough_tex_ref, 1, 1)
        transmiss_tex_info  = tex_exp.TextureInfo(transmiss_tex_ref, 1, 1)
        alpha_tex_info      = tex_exp.TextureInfo(alpha_tex_ref, 1, 1)
        bump_tex_info       = tex_exp.TextureInfo(bump_tex_ref, 1, 1)
        emiss_col_tex_info  = tex_exp.TextureInfo(emiss_col_tex_ref, 1, 1)
        emiss_col_tex_info  = tex_exp.TextureInfo(emiss_col_tex_ref, 1, 1)

        has_metallic_tex  = metallic_tex_ref.id >= 0

        if engine.version_engine == "HYDRA ENGINE 2":
            cast_shadow    = material.hydra_render.cast_shadow
            smooth_opacity = material.hydra_render.smooth_opacity
        if engine.version_engine == "HYDRA ENGINE 3":
            cast_shadow    = True
            smooth_opacity = False


        mat_ref = hy.hrMaterialCreate(material.name)
        hy.hrMaterialOpen(mat_ref, hy.HR_WRITE_DISCARD)

        if metallic < 1 or has_metallic_tex:
            # Starting with Blender 4.0 there is a special case for turning off reflections
            only_diffuse = ior == 0 
            
            if only_diffuse:
                add_diffuse_xml_node(engine, mat_ref, base_color, 0, base_tex_info)
                add_bump_xml_node(engine, mat_ref, bump_tex_info)
                add_emission_xml_node(engine, mat_ref, emiss_color, emission, False, emiss_col_tex_info)
                add_opacity_xml_node(engine, mat_ref, cast_shadow, smooth_opacity, alpha_tex_info)
            else:                          
                if spec_ior != 0.5 and spec_ior > 0:
                    ior = util.lerp(1.1, 1.8, spec_ior) # https://youtu.be/7ucxCoa7jq0?t=771

                color_diffuse = util.mult_color(base_color, 1.0 - transmiss)
                add_plastic_xml_node(engine, mat_ref, color_diffuse, refl_rough, ior, base_tex_info, None, refl_rough_tex_info) 

                has_transp_tex = transmiss_tex_info != None and transmiss_tex_info.ref.id >= 0
                if transmiss > 0 or has_transp_tex:
                    thin_walled    = False
                    fog_color      = (1,1,1)
                    fog_multiplier = 1.0
                    transmiss     += int(has_transp_tex)
                    color_transp   = util.mult_color(base_color, transmiss)
                    add_refraction_hydra2_xml_node(mat_ref, color_transp, refl_rough, ior, thin_walled, fog_color, fog_multiplier, 
                                        transmiss_tex_info, refl_rough_tex_info)
                
                add_bump_xml_node(engine, mat_ref, bump_tex_info)
                add_emission_xml_node(engine, mat_ref, emiss_color, emission, False, emiss_col_tex_info)
                add_opacity_xml_node(engine, mat_ref, cast_shadow, smooth_opacity, alpha_tex_info)
            hy.hrMaterialClose(mat_ref)

            if metallic > 0 or has_metallic_tex:
                mat2_metall_ref = hy.hrMaterialCreate(f"{material.name}_metall_part")
                hy.hrMaterialOpen(mat2_metall_ref, hy.HR_WRITE_DISCARD)
                add_metall_xml_node(engine, mat2_metall_ref, base_color, refl_rough, refl_rough, 100, 1, base_tex_info, refl_rough_tex_info)
                add_bump_xml_node(engine, mat_ref, bump_tex_info)
                add_emission_xml_node(engine, mat_ref, emiss_color, emission, False, emiss_col_tex_info)
                add_opacity_xml_node(engine, mat_ref, cast_shadow, smooth_opacity, alpha_tex_info)
                hy.hrMaterialClose(mat2_metall_ref)

                mat_blend = hy.hrMaterialCreateBlend(f"{material.name}_blend", mat_ref, mat2_metall_ref)
                create_blend_2_materials(engine, mat_blend, mat_ref, mat2_metall_ref, metallic, metallic_tex_info)
                mat_ref = mat_blend
        else:                        
            add_metall_xml_node(engine, mat_ref, base_color, refl_rough, refl_rough, 100, 1, base_tex_info, refl_rough_tex_info)
            add_bump_xml_node(engine, mat_ref, bump_tex_info)
            add_emission_xml_node(engine, mat_ref, emiss_color, emission, False, emiss_col_tex_info)
            add_opacity_xml_node(engine, mat_ref, cast_shadow, smooth_opacity, alpha_tex_info)
            hy.hrMaterialClose(mat_ref)


        output_dict["mat_ref"] = mat_ref

    ##################################################################
        
    elif node.bl_idname == "ShaderNodeMixShader":
        pass
    elif node.bl_idname == "ShaderNodeBsdfDiffuse":
        color_socket = _socket(engine, node.inputs["Color"],     material, obj_name, group_node_stack)
        rough_socket = _socket(engine, node.inputs["Roughness"], material, obj_name, group_node_stack)
        bump_socket  = _socket(engine, node.inputs["Normal"], material, obj_name, group_node_stack)

        color        = color_socket.get("value", [1, 1, 1])
        roughness    = rough_socket.get("value", 0)
        tex_ref      = color_socket.get("tex_ref", hy.HRTextureNodeRef())
        bump_tex_ref = bump_socket.get("tex_ref", hy.HRTextureNodeRef())
   
        base_tex_info = tex_exp.TextureInfo(tex_ref, 1, 1)
        bump_tex_info = tex_exp.TextureInfo(bump_tex_ref, 1, 1)

        mat_ref       = hy.hrMaterialCreate(material.name)
        hy.hrMaterialOpen(mat_ref, hy.HR_WRITE_DISCARD)
        add_diffuse_xml_node(engine, mat_ref, color, roughness, base_tex_info)
        add_bump_xml_node(engine, mat_ref, bump_tex_info)
        hy.hrMaterialClose(mat_ref)
        output_dict["mat_ref"] = mat_ref

    ##################################################################

    elif node.bl_idname == "ShaderNodeBsdfGlossy":
        pass
    # elif node.bl_idname == "ShaderNodeBackground":
    #     pass
    elif node.bl_idname == "ShaderNodeTexImage":
        if node.image:
            extension_map = {
                "REPEAT": "repeat",
                "EXTEND": "clamp",
                "CLIP": "black",
            }

            try:
                if node.image.source == "FILE":
                    filepath = util.get_abspath(node.image.filepath, library=node.image.library,
                                                must_exist=True, must_be_existing_file=True)

                    tex_ref = hy.hrTexture2DCreateFromFile(filepath)
                    output_dict["tex_ref"] = tex_ref

                # TODO: texture properties, sampler, ...
                # "wrap": extension_map[node.extension],
                # "channel": "alpha" if output_socket == node.outputs["Alpha"] else "rgb",
                # "gamma": 2.2 if node.image.colorspace_settings.name == "sRGB" else 1,

            except OSError as error:
                util.report(engine, "DEBUG", f"ShaderNodeTexImage {obj_name} error: {error}")
                return MISSING_TEXTURE_COLOR
        else:
            return MISSING_TEXTURE_COLOR
        pass

    ##################################################################
    
    elif node.bl_idname == "ShaderNodeBsdfGlass":
        color_socket   = _socket(engine, node.inputs["Color"], material, obj_name, group_node_stack)
        rough_socket   = _socket(engine, node.inputs["Roughness"], material, obj_name, group_node_stack)
        ior_socket     = _socket(engine, node.inputs["IOR"], material, obj_name, group_node_stack)
        bump_socket    = _socket(engine, node.inputs["Normal"], material, obj_name, group_node_stack)
  
        color          = color_socket.get("value", [1, 1, 1])        
        ior            = ior_socket.get("value", 1.5)        
        roughness      = 0
        thin_walled    = False
        fog_color      = (1, 1, 1)
        fog_multiplier = 1.0

        color_tex_ref = color_socket.get("tex_ref", hy.HRTextureNodeRef())
        rough_tex_ref = rough_socket.get("tex_ref", hy.HRTextureNodeRef())
        bump_tex_ref  = bump_socket.get("tex_ref", hy.HRTextureNodeRef())

        color_tex_info = tex_exp.TextureInfo(color_tex_ref, 1, 1)
        rough_tex_info = tex_exp.TextureInfo(rough_tex_ref, 1, 1)
        bump_tex_info  = tex_exp.TextureInfo(bump_tex_ref, 1, 1)

        if engine.version_engine == "HYDRA ENGINE 2":
            roughness      = rough_socket.get("value", 0)
            fog_color      = material.hydra_render.fog_color
            fog_multiplier = material.hydra_render.fog_multiplier
            thin_walled    = material.hydra_render.thin_walled
            smooth_opacity = material.hydra_render.smooth_opacity
            cast_shadow    = material.hydra_render.cast_shadow


        mat_ref    = hy.hrMaterialCreate(material.name)
        hy.hrMaterialOpen(mat_ref, hy.HR_WRITE_DISCARD)

        add_glass_xml_node(engine, mat_ref, color, roughness, ior, thin_walled, fog_color, fog_multiplier, 
                           color_tex_info, rough_tex_info)
        add_bump_xml_node(engine, mat_ref, bump_tex_info)
        add_opacity_xml_node(engine, mat_ref, cast_shadow, smooth_opacity)

        hy.hrMaterialClose(mat_ref)
        output_dict["mat_ref"] = mat_ref

    ##################################################################

    elif node.bl_idname == "ShaderNodeBsdfRefraction":
        pass
    elif node.bl_idname == "ShaderNodeBsdfAnisotropic":
       pass
    elif node.bl_idname == "ShaderNodeBsdfTranslucent":
       pass
    elif node.bl_idname == "ShaderNodeBsdfTransparent":
       pass
    elif node.bl_idname == "ShaderNodeHoldout":
        pass
    elif node.bl_idname == "ShaderNodeMixRGB":
        pass
    elif node.bl_idname == "ShaderNodeMath":
       pass
    elif node.bl_idname == "ShaderNodeHueSaturation":
       pass
    elif node.bl_idname == "ShaderNodeGroup":
        active_output = None
        for subnode in node.node_tree.nodes:
            if subnode.bl_idname == "NodeGroupOutput" and subnode.is_active_output:
                active_output = subnode
                break

        current_input = active_output.inputs[output_socket.name]
        if not current_input.is_linked:
            return 0

        link = util.get_link(current_input)

        if group_node_stack is None:
            _group_node_stack = []
        else:
            _group_node_stack = group_node_stack.copy()

        _group_node_stack.append(node)

        return _node(engine, link.from_node, link.from_socket, material, obj_name, _group_node_stack)
    elif node.bl_idname == "NodeGroupInput":
        return _socket(engine, group_node_stack[-1].inputs[output_socket.name], material, obj_name, group_node_stack[:-1])
    elif node.bl_idname == "ShaderNodeEmission":
        pass
    elif node.bl_idname == "ShaderNodeValue":
        pass
    elif node.bl_idname == "ShaderNodeRGB":
        pass
    elif node.bl_idname == "ShaderNodeValToRGB": # Color ramp
        pass
    elif node.bl_idname == "ShaderNodeTexChecker":
        pass
    elif node.bl_idname == "ShaderNodeInvert":
        pass
    elif node.bl_idname in {"ShaderNodeSeparateRGB", "ShaderNodeSeparateXYZ"}:
       pass
    elif node.bl_idname in {"ShaderNodeCombineRGB", "ShaderNodeCombineXYZ"}:
        pass
    elif node.bl_idname == "ShaderNodeRGBToBW":
        pass
    elif node.bl_idname == "ShaderNodeBrightContrast":
       pass
    elif node.bl_idname == "ShaderNodeNormalMap":
        pass
    elif node.bl_idname == "ShaderNodeBump":
       pass
    elif node.bl_idname == "ShaderNodeNewGeometry":
        pass
    elif node.bl_idname == "ShaderNodeObjectInfo":
       pass
    elif node.bl_idname == "ShaderNodeBlackbody":
       pass
    elif node.bl_idname == "ShaderNodeMapRange":
       pass
    else:
        util.report(engine, "DEBUG", f"Unsupported node type: {node.name}")

        # if node.internal_links:
        #     links = node.internal_links[0].from_socket.links
        #     if links:
        #         link = links[0]
        #         print("current node", node.name, "failed, testing next node:", link.from_node.name)
        #         return _node(link.from_node, link.from_socket, props, material, luxcore_name, obj_name, group_node_stack)

        return 0

    return output_dict




def add_diffuse_xml_node(a_engine, a_mat_ref, a_color, a_roughness, a_tex_info = None):
    mat_node   = hy.hrMaterialParamNode(a_mat_ref)

    color_val = util.array_round_to_string(a_color, 4)
    rough_val = util.round_to_string(a_roughness, 4)

    color_node = None

    if a_engine.version_engine == "HYDRA ENGINE 2":
        diff_node = mat_node.append_child("diffuse      ")
        brdf_type = "lambert"
        if a_roughness > 0:
            brdf_type = "orennayar"
            diff_node.append_child("roughness  ").force_attribute("val").set_value(rough_val)

        diff_node.force_attribute("brdf_type").set_value(brdf_type)
        color_node = diff_node.append_child("color      ")
        color_node.force_attribute("val").set_value(color_val)
    
    elif a_engine.version_engine == "HYDRA ENGINE 3":
        mat_node.force_attribute("type").set_value("diffuse")
        brdf_type = "lambert"
        if a_roughness > 0:
            brdf_type = "oren-nayar"
            mat_node.append_child("roughness  ").force_attribute("val").set_value(rough_val)

        mat_node.append_child("bsdf       ").force_attribute("type").set_value(brdf_type)
        color_node = mat_node.append_child("reflectance")
        color_node.force_attribute("val").set_value(color_val)

    if a_tex_info != None and a_tex_info.ref.id >= 0:
        a_tex_info.bind_texture(color_node)



def add_reflection_hydra2_xml_node(a_mat_ref, a_refl_color, a_roughness, a_ior, a_refl_tex_info = None, a_rough_tex_info = None):
    mat_node   = hy.hrMaterialParamNode(a_mat_ref)        
    refl_node  = mat_node.append_child("reflectivity ")    
    refl_node.force_attribute("brdf_type").set_value("ggx")        
    color_node = refl_node.append_child("color      ")
    color_node.force_attribute("val").set_value(util.array_round_to_string(a_refl_color, 4))                
    gloss_node = refl_node.append_child("glossiness ")
    gloss_node.force_attribute("val").set_value(util.round_to_string(util.clamp(1.0 - a_roughness, 0.0, 1.0), 4))
    refl_node.append_child("extrusion  ").force_attribute("val").set_value("maxcolor")
    refl_node.append_child("fresnel    ").force_attribute("val").set_value("1")
    refl_node.append_child("fresnel_ior").force_attribute("val").set_value(util.round_to_string(a_ior, 4))
    refl_node.append_child("energy_fix ").force_attribute("val").set_value("1")
    
    if a_refl_tex_info != None and a_refl_tex_info.ref.id >= 0:
        a_refl_tex_info.bind_texture(color_node)

    if a_rough_tex_info != None and a_rough_tex_info.ref.id >= 0:
        a_rough_tex_info.bind_texture(gloss_node)



def add_refraction_hydra2_xml_node(a_mat_ref, a_refl_color, a_roughness, a_ior, a_thin_walled, a_fog_color, a_fog_multiplier, 
                                   a_refr_tex_info = None, a_rough_tex_info = None):
    mat_node   = hy.hrMaterialParamNode(a_mat_ref)        
    refl_node  = mat_node.append_child("transparency ")    
    refl_node.force_attribute("brdf_type").set_value("ggx")        
    color_node = refl_node.append_child("color         ")
    color_node.force_attribute("val").set_value(util.array_round_to_string(a_refl_color, 4))                
    gloss_node = refl_node.append_child("glossiness    ")
    gloss_node.force_attribute("val").set_value(util.round_to_string(util.clamp(1.0 - a_roughness, 0.0, 1.0), 4))
    refl_node.append_child("extrusion     ").force_attribute("val").set_value("maxcolor")
    refl_node.append_child("fresnel       ").force_attribute("val").set_value("1")
    refl_node.append_child("ior           ").force_attribute("val").set_value(util.round_to_string(a_ior, 4))
    refl_node.append_child("thin_walled   ").force_attribute("val").set_value(a_thin_walled)
    refl_node.append_child("fog_color     ").force_attribute("val").set_value(util.array_round_to_string(a_fog_color, 4))      
    refl_node.append_child("fog_multiplier").force_attribute("val").set_value(util.round_to_string(a_fog_multiplier, 4))
    refl_node.append_child("energy_fix    ").force_attribute("val").set_value("1")
    
    if a_refr_tex_info != None and a_refr_tex_info.ref.id >= 0:
        a_refr_tex_info.bind_texture(color_node)

    if a_rough_tex_info != None and a_rough_tex_info.ref.id >= 0:
        a_rough_tex_info.bind_texture(gloss_node)



def add_plastic_xml_node(a_engine, a_mat_ref, a_diffuse_color, a_refl_rough, a_ior, a_diff_tex_info = None,
                         a_refl_tex_info = None, a_rough_tex_info = None):
    mat_node   = hy.hrMaterialParamNode(a_mat_ref)
    
    if a_engine.version_engine == "HYDRA ENGINE 2":
        add_diffuse_xml_node(a_engine, a_mat_ref, a_diffuse_color, 0, a_diff_tex_info)        
        add_reflection_hydra2_xml_node(a_mat_ref, [1, 1, 1], a_refl_rough, a_ior, a_refl_tex_info, a_rough_tex_info)

    elif a_engine.version_engine == "HYDRA ENGINE 3":        
        mat_node.force_attribute("type").set_value("plastic")
        color_node = mat_node.append_child("reflectance    ")
        color_node.force_attribute("val").set_value(util.array_round_to_string(a_diffuse_color, 4))
        mat_node.append_child("int_ior        ").force_attribute("val").set_value(util.round_to_string(a_ior, 4))
        mat_node.append_child("ext_ior        ").force_attribute("val").set_value("1.000277")
        rough_node = mat_node.append_child("alpha          ")
        rough_node.append_attribute("val").set_value(util.round_to_string(a_refl_rough, 4))
        mat_node.append_child("square_rough_on").force_attribute("val").set_value("1")
        mat_node.append_child("nonlinear      ").force_attribute("val").set_value("0")

        if a_diff_tex_info != None and a_diff_tex_info.ref.id >= 0:
            a_diff_tex_info.bind_texture(color_node)

        if a_rough_tex_info != None and a_rough_tex_info.ref.id >= 0:
            a_rough_tex_info.bind_texture(rough_node)



def add_metall_xml_node(a_engine, a_mat_ref, a_refl_color, a_alpha_u, a_alpha_v, a_eta = 100, k = 1,
                        a_refl_tex_info = None, a_rough_tex_info = None):
    mat_node = hy.hrMaterialParamNode(a_mat_ref)

    if a_engine.version_engine == "HYDRA ENGINE 2":
        add_reflection_hydra2_xml_node(a_mat_ref, a_refl_color, a_alpha_u, 100, a_refl_tex_info, a_rough_tex_info)

    elif a_engine.version_engine == "HYDRA ENGINE 3":        
        mat_node.force_attribute("type").set_value("rough_conductor")
        mat_node.append_child("bsdf           ").force_attribute("val").set_value("ggx")
        color_node = mat_node.append_child("reflectance    ")
        color_node.force_attribute("val").set_value(util.array_round_to_string(a_refl_color, 4))
        
        rough_node = None
        if a_rough_tex_info != None:
            rough_node = mat_node.append_child("alpha          ")
            rough_node.append_attribute("val").set_value(util.round_to_string(a_alpha_u, 4))
        else:
            mat_node.append_child("alpha_u        ").force_attribute("val").set_value(util.round_to_string(a_alpha_u, 4))
            mat_node.append_child("alpha_v        ").force_attribute("val").set_value(util.round_to_string(a_alpha_v, 4))

        mat_node.append_child("square_rough_on").force_attribute("val").set_value("1")
        mat_node.append_child("eta            ").force_attribute("val").set_value(util.round_to_string(a_eta, 4))
        mat_node.append_child("k              ").force_attribute("val").set_value(util.round_to_string(k, 4))

        if a_refl_tex_info != None and a_refl_tex_info.ref.id >= 0:
            a_refl_tex_info.bind_texture(color_node)

        if a_rough_tex_info != None and a_rough_tex_info.ref.id >= 0:
            a_rough_tex_info.bind_texture(rough_node)



def add_glass_xml_node(a_engine, a_mat_ref, a_color, a_roughness, a_ior, a_thin_walled, a_fog_color, a_fog_multiplier, 
                       a_color_tex_info = None, a_rough_tex_info = None):
    mat_node = hy.hrMaterialParamNode(a_mat_ref)

    if a_engine.version_engine == "HYDRA ENGINE 2":
        add_reflection_hydra2_xml_node(a_mat_ref, a_color, a_roughness, a_ior, a_color_tex_info, a_rough_tex_info)
        add_refraction_hydra2_xml_node(a_mat_ref, a_color, a_roughness, a_ior, a_thin_walled, a_fog_color, a_fog_multiplier, 
                                       a_color_tex_info, a_rough_tex_info)

    elif a_engine.version_engine == "HYDRA ENGINE 3":        
        mat_node.force_attribute("type").set_value("dielectric")
        mat_node.append_child("int_ior        ").force_attribute("val").set_value(util.round_to_string(a_ior, 4))
        mat_node.append_child("ext_ior        ").force_attribute("val").set_value("1.0")



def create_blend_2_materials(a_engine, a_mat_blend, a_mat1_ref, a_mat2_ref, a_weight, a_weight_tex_info = None):
    hy.hrMaterialOpen(a_mat_blend, hy.HR_WRITE_DISCARD)
  
    mat_node = hy.hrMaterialParamNode(a_mat_blend)

    has_blend_texture = a_weight_tex_info != None and a_weight_tex_info.ref.id >= 0

    if has_blend_texture:
        a_weight = 1

    if a_engine.version_engine == "HYDRA ENGINE 2":
        blend_node = mat_node.append_child("blend")
        blend_node.force_attribute("type").set_value("mask_blend")
        mask_node  = blend_node.append_child("mask")
        mask_node.append_attribute("val").set_value(util.round_to_string(a_weight, 4))
        
    elif a_engine.version_engine == "HYDRA ENGINE 3":   
        mat_node.force_attribute("type").set_value("blend")     
        mask_node = mat_node.append_child("weight         ")
        mask_node.append_attribute("val").set_value(util.round_to_string(a_weight, 4))   
        mat_node.append_child("bsdf_1         ").force_attribute("id ").set_value(a_mat1_ref.id)
        mat_node.append_child("bsdf_2         ").force_attribute("id ").set_value(a_mat2_ref.id)

    if has_blend_texture:
        a_weight_tex_info.bind_texture(mask_node)

    hy.hrMaterialClose(a_mat_blend)



def add_bump_xml_node(a_engine, a_mat_ref, a_bump_tex_info):
    if a_bump_tex_info != None and a_bump_tex_info.ref.id >= 0:
        mat_node        = hy.hrMaterialParamNode(a_mat_ref)
        displ_node      = mat_node.force_child("displacement")
        displ_node.force_attribute("type").set_value("normal_bump")    
        normal_map_node = displ_node.force_child("normal_map")
        invert_node     = normal_map_node.force_child("invert")
        invert_node.force_attribute("x").set_value("0")
        invert_node.force_attribute("y").set_value("0")
        invert_node.force_attribute("swap_xy").set_value("0")
        a_bump_tex_info.bind_texture(normal_map_node, True)



def add_emission_xml_node(a_engine, a_mat_ref, a_emiss_color, a_emiss_val, a_cast_gi, a_emiss_col_tex_info):
    if a_engine.version_engine == "HYDRA ENGINE 2" and a_emiss_val > 0:        
        mat_node     = hy.hrMaterialParamNode(a_mat_ref)
        emiss_node   = mat_node.force_child("emission      ")
        color        = util.mult_color(a_emiss_color, a_emiss_val)
        color_node   = emiss_node.force_child("color      ")
        color_node.force_attribute("val").set_value(util.array_round_to_string(color, 4))
        emiss_node.force_child("cast_gi    ").force_attribute("val").set_value(int(a_cast_gi))
        
        if a_emiss_col_tex_info != None and a_emiss_col_tex_info.ref.id >= 0:
            a_emiss_col_tex_info.bind_texture(color_node)

    

def add_opacity_xml_node(a_engine, a_mat_ref, a_cast_shadow, a_smooth, a_opacity_tex_info = None):
    if a_engine.version_engine == "HYDRA ENGINE 2":
        has_texture = a_opacity_tex_info != None and a_opacity_tex_info.ref.id >= 0

        if a_cast_shadow is False or has_texture:
            mat_node     = hy.hrMaterialParamNode(a_mat_ref)
            opacity_node = mat_node.force_child("opacity      ")
            
            if a_cast_shadow is False:
                opacity_node.force_child("skip_shadow   ").force_attribute("val").set_value(1)
            else:
                opacity_node.remove_child("skip_shadow   ")        
                
            if has_texture:
                opacity_node.force_attribute("smooth").set_value(int(a_smooth))
                a_opacity_tex_info.bind_texture(opacity_node)

