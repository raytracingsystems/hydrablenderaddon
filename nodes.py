import bpy


class HydraNodeTree(bpy.types.NodeTree):
    '''Hydra Material Nodes'''

    bl_idname = 'HydraNodeTree'
    bl_label = 'Hydra Shader Nodes'
    bl_icon = 'NODETREE'

    @classmethod
    def poll(cls, context):
        return context.scene.render.engine == 'CYCLES_RENDER'

    def update(self):
        self.refresh = True


class HydraOutputNode(bpy.types.ShaderNode):
    '''Hydra Shader Output'''

    bl_idname = 'HydraOutputNode'
    bl_label = 'Hydra Shader Output'
    bl_icon = 'NODE'

    def init(self, context):
        self.inputs.new('HydraSocketBRDF', "Hydra BRDF")

    def draw_buttons(self, context, layout):
        pass

    def draw_buttons_ext(self, context, layout):
        pass

    def draw_label(self):
        return "Output"


class HydraDiffuseNode(bpy.types.ShaderNode):
    '''Hydra Diffuse BRDF'''

    bl_idname = 'HydraDiffuseNode'
    bl_label  = 'Hydra Diffuse'
    bl_icon   = 'NODE'

    def init(self, context):
        intensity = self.inputs.new('HydraSocketFloat_0_1', "Multiplier")
        intensity.default_value = 1.0

        color = self.inputs.new('HydraSocketColor', "Color")
        color.default_value = 1.0, 1.0, 1.0

        self.outputs.new('HydraSocketBRDF', "Hydra Diffuse BRDF")

    def draw_label(self):
        return "Diffuse"

