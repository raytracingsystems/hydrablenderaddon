# <pep8 compliant>

from bl_operators.presets import AddPresetBase
from bpy.types import Operator


class AddPresetIntegrator(AddPresetBase, Operator):
    '''Add an Integrator Preset'''
    bl_idname   = "render.hydra_integrator_preset_add"
    bl_label    = "Add Integrator Preset"
    preset_menu = "HYDRA_PT_integratorPresets"

    preset_defines = [
        "hydra = bpy.context.scene.hydra_render"
    ]

    preset_values = [
        "hydra.max_bounces",
        "hydra.diffuse_bounces",
        "hydra.caustics",
    ]

    preset_subdir = "hydra/integrator"


class AddPresetSampling(AddPresetBase, Operator):
    '''Add a Sampling Preset'''
    bl_idname   = "render.hydra_sampling_preset_add"
    bl_label    = "Add Sampling Preset"
    preset_menu = "HYDRA_PT_samplingPresets"

    preset_defines = [
        "hydra = bpy.context.scene.hydra_render"
    ]

    preset_values = [
        "hydra.samples",
        "hydra.preview_samples",
        "hydra.aa_samples",
        "hydra.preview_aa_samples",
        "hydra.diffuse_samples",
        "hydra.glossy_samples",
        "hydra.transmission_samples",
        "hydra.ao_samples",
        "hydra.mesh_light_samples",
        "hydra.subsurface_samples",
        "hydra.volume_samples",
        "hydra.use_square_samples",
        "hydra.integrator",
        "hydra.seed",
        "hydra.sample_clamp_direct",
        "hydra.sample_clamp_indirect",
        "hydra.sample_all_lights_direct",
        "hydra.sample_all_lights_indirect",
    ]

    preset_subdir = "hydra/sampling"


classes = (
    AddPresetIntegrator,
    AddPresetSampling,
)


def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)


def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)


if __name__ == "__main__":
    register()
