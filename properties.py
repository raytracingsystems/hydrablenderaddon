import bpy
import math
from bpy.props import (
    BoolProperty,
    # CollectionProperty,
    EnumProperty,
    FloatProperty,
    FloatVectorProperty,
    IntProperty,
    PointerProperty,
    StringProperty,
)



enum_devices = (
    ('CPU', "CPU", "Use CPU for rendering"),
    ('GPU', "GPU Compute", "Use GPU compute device for rendering, configured in the system tab in the user preferences"),
)

enum_device_type = (
    ('CPU', "CPU", "CPU", 0),
    ('OPENCL', "OpenCL", "OpenCL", 1)
)


enum_feature_set = (
    ('SUPPORTED', "Supported", "Only use finished and supported features"),
    ('EXPERIMENTAL', "Experimental", "Use experimental and incomplete features that might be broken or change in the future", 'ERROR', 1),
)

enum_version_engine = (
    ('HYDRA ENGINE 2', "Hydra engine 2", "Version of the rendering engine"),
    ('HYDRA ENGINE 3', "Hydra engine 3", "Version of the rendering engine"),
)

enum_integrator_engine_2 = (
    ('PATH', "Path Tracing", "Pure path tracing integrator"),
    ('LT', "Light Tracing", "The light trace integrator emits rays from the light source"),
    ('IBPT', "IBPT", "Instant bidirectional path tracing. A mixture of PT and LT, takes the best of two strategies"),
    ('MMLT', "MMLT", "Multiplexed metropolis light transport. Complex adaptive algorithm, for the most complex lighting"),
)

enum_integrator_engine_3 = (
    ('PATH', "Path Tracing", "Pure path tracing integrator"),    
)

enum_displacement_methods = (
    ('BUMP', "Bump Only", "Bump mapping to simulate the appearance of displacement"),
    ('DISPLACEMENT', "Displacement Only", "Use true displacement of surface only, requires fine subdivision"),
    ('BOTH', "Displacement and Bump", "Combination of true displacement and bump mapping for finer detail"),
)

enum_view3d_shading_render_pass = (
    ('', "General", ""),

    ('COMBINED', "Combined", "Show the Combined Render pass", 1),
    ('EMISSION', "Emission", "Show the Emission render pass", 33),
    ('BACKGROUND', "Background", "Show the Background render pass", 34),
    ('AO', "Ambient Occlusion", "Show the Ambient Occlusion render pass", 35),

    ('', "Light", ""),

    ('DIFFUSE_DIRECT', "Diffuse Direct", "Show the Diffuse Direct render pass", 38),
    ('DIFFUSE_INDIRECT', "Diffuse Indirect", "Show the Diffuse Indirect render pass", 39),
    ('DIFFUSE_COLOR', "Diffuse Color", "Show the Diffuse Color render pass", 40),

    ('GLOSSY_DIRECT', "Glossy Direct", "Show the Glossy Direct render pass", 41),
    ('GLOSSY_INDIRECT', "Glossy Indirect", "Show the Glossy Indirect render pass", 42),
    ('GLOSSY_COLOR', "Glossy Color", "Show the Glossy Color render pass", 43),

    ('', "", ""),

    ('TRANSMISSION_DIRECT', "Transmission Direct", "Show the Transmission Direct render pass", 44),
    ('TRANSMISSION_INDIRECT', "Transmission Indirect", "Show the Transmission Indirect render pass", 45),
    ('TRANSMISSION_COLOR', "Transmission Color", "Show the Transmission Color render pass", 46),

    ('VOLUME_DIRECT', "Volume Direct", "Show the Volume Direct render pass", 50),
    ('VOLUME_INDIRECT', "Volume Indirect", "Show the Volume Indirect render pass", 51),

    ('', "Data", ""),

    ('NORMAL', "Normal", "Show the Normal render pass", 3),
    ('UV', "UV", "Show the UV render pass", 4),
    ('MIST', "Mist", "Show the Mist render pass", 32),
)


def enum_openimagedenoise_denoiser(self, context):
    # import _hydra
    # if _hydra.with_openimagedenoise:
        return [('OPENIMAGEDENOISE', "OpenImageDenoise", "Use Intel OpenImageDenoise AI denoiser running on the CPU", 4)]
    # return []


def enum_preview_denoiser(self, context):
    oidn_items = enum_openimagedenoise_denoiser(self, context)

    if len(oidn_items):
        items = [('AUTO', "Automatic", "Use the fastest available denoiser for viewport rendering (OptiX if available, OpenImageDenoise otherwise)", 0)]
    else:
        items = [('AUTO', "None", "Blender was compiled without a viewport denoiser", 0)]

    items += oidn_items
    return items


def enum_denoiser(self, context):
    items = [('NLM', "NLM", "hydra native non-local means denoiser, running on any compute device", 1)]
    items += enum_openimagedenoise_denoiser(self, context)
    return items


enum_denoising_input_passes = (
    ('RGB', "Color", "Use only color as input", 1),
    ('RGB_ALBEDO', "Color + Albedo", "Use color and albedo data as input", 2),
    ('RGB_ALBEDO_NORMAL', "Color + Albedo + Normal", "Use color, albedo and normal data as input", 3),
)


def update_render_passes(self, context):
    # scene      = context.scene
    view_layer = context.view_layer
    view_layer.update_render_passes()
    # engine.detect_conflicting_passes(scene, view_layer)


class HydraRenderSettings(bpy.types.PropertyGroup):

    device: EnumProperty(
        name        = "Device",
        description = "Device to use for rendering",
        items       = enum_devices,
        default     = 'GPU',
    )

    feature_set: EnumProperty(
        name        = "Feature Set",
        description = "Feature set to use for rendering",
        items       = enum_feature_set,
        default     = 'SUPPORTED',
    )

    # Quality


    samples: IntProperty(
        name        = "Samples",
        description = "Number of samples per pixel",
        soft_min    = 64, soft_max = 100000,
        min         = 1, max = 1000000,
        default     = 512,
    )

    ray_bounces: IntProperty(
        name        = "Ray Bounces",
        description = "The maximum number of bounces of any rays",
        min         = 0, max = 255,
        default     = 10,
    )

    diffuse_bounces: IntProperty(
        name        = "Diffuse Bounces",
        description = "Maximum number of diffuse bounces only",
        min         = 0, max = 255,
        default     = 8,
    )

    use_limit_render_time: BoolProperty(
        name        = "Enable Limit Render Time",
        description = "Enable limit rendering time in minutes",
        default     = False,
    )

    limit_render_time: IntProperty(
        name        = "Limit Render Time",
        description = "Limit rendering time in minutes",
        min         = 0, max = 10000,
        default     = 1,
    )

    # Engine

    version_engine: EnumProperty(
        name        = "Version",
        description = "Version of the rendering engine",
        items       = enum_version_engine,
        default     = 'HYDRA ENGINE 2',
    )

    integrator_engine_2: EnumProperty(
        name        = "Integrator",
        description = "Method to sample lights and materials",
        items       = enum_integrator_engine_2,
        default     = 'PATH',
    )

    integrator_engine_3: EnumProperty(
        name        = "Integrator",
        description = "Method to sample lights and materials",
        items       = enum_integrator_engine_3,
        default     = 'PATH',
    )

    # Engine: Path Tracing

    use_caustics: BoolProperty(
        name        = "Reflective Caustics",
        description = "Use reflective caustics, resulting in a brighter image (more noise but added realism)",
        default     = True,
    )

    fast_background: BoolProperty(
        name        = "Fast Background",
        description = "The mode accelerates rendering in scenes with large areas without geometry, for example, a car or a building in the background of the environment",
        default     = False,
    )

    auto_texture_resize: BoolProperty(
        name        = "Auto Texture Resize",
        description = "Automatically reduces the resolution of textures, depending on their location in the scene and the resolution of the render, which allows you to save a lot of memory",
        default     = False,
    )

    spectral_mode: BoolProperty(
        name        = "Enable spectral rendering",
        description = "Spectral rendering will require spectral textures for materials and lighting in SPD format",
        default     = False,
    )

    use_bsdf_clamp: BoolProperty(
        name        = "Use BSDF clamp",
        description = "Use BSDF clamp",
        default     = False,
    )

    bsdf_clamp: FloatProperty(
        name        = "BSDF clamp",
        description = "The maximum value of the brightness of the reflected rays from the materials. Suppresses firefly",
        min         = 1.0, soft_max = 100.0, max = 1000000.0,
        step        = 1.0,
        default     = 5.0,
    )

    use_qmc_lights: BoolProperty(
        name        = "Use QMC light sampling",
        description = "Sampling of light and shadow sources using the quasi monte Carlo method",
        default     = False,
    )

    use_qmc_materials: BoolProperty(
        name        = "Use QMC materials sampling",
        description = "Sampling of materials by the quasi monte Carlo method",
        default     = False,
    )

    # Engine: MMLT

    mmlt_estimate_time: IntProperty(
        name        = "Estimate Time",
        description = "The approximate time in hours that the Path tracing algorithm would need for the final quality (for Nvidia GTX 1060)",
        min         = 0, max = 4,
        default     = 1,
    )

    use_mmlt_median_filter: BoolProperty(
        name        = "Use median filter",
        description = "Enabling the median filter only for secondary MMLT bounces, to reduce its noise",
        default     = False,
    )

    mmlt_median_filter_threshold: FloatProperty(
        name        = "Threshold",
        description = "A pixel that differs by this value from those surrounding it will be filtered",
        min         = 0.0, max = 0.5,
        step        = 0.1,
        default     = 0.1,
    )

    # Override

    use_gray_materials: BoolProperty(
        name        = "Use Gray Materials",
        description = "Replaces all materials except those with non-zero transparency, such as glass, with a gray material with RGB color (128, 128, 128) during rendering, for evaluating and adjusting lighting",
        default     = False,
    )

    envir_mult: FloatProperty(
        name        = "Environment Multiplier",
        description = "",
        min         = 0.0, max = 1000.0,
        step        = 0.1,
        default     = 1.0,
    )

    # Denoise

    use_denoising: BoolProperty(
        name        = "Use Denoising",
        description = "Denoise the rendered image",
        default     = False,
    )

    denoiser: EnumProperty(
        name        = "Denoiser",
        description = "Denoise the image with the selected denoiser. "
        "For denoising the image after rendering, denoising data render passes "
        "also adapt to the selected denoiser",
        items       = enum_denoiser,
        default     = 1,
        update      = update_render_passes,
    )



class HydraCameraSettings(bpy.types.PropertyGroup):

    def update_enable_dof(self, context):
        camera             = self.id_data
        camera.dof.use_dof = self.enable_dof

    def update_dof_lens_radius(self, context):
        # A simple approximation of the viewport to hydra with the inverse proportionality formula
        # hydra dof_lens_radius  = [0.5, 0.25]
        # blender aperture_fstop ~ [0.1, 0.2]
        # blender = k / hydra
        # k = 0.5 * 0.1
        camera                    = self.id_data
        camera.dof.aperture_fstop = 0.05 / self.dof_lens_radius

    def update_dof_focus_distance(self, context):
        camera                    = self.id_data
        camera.dof.focus_distance = self.dof_focus_distance


    enable_dof: BoolProperty(
        name        = "Enable DOF",
        description = "Enable depth of field",
        update      = update_enable_dof,
        default     = False,
    )

    dof_lens_radius: FloatProperty(
        name        = "DOF lens radius",
        description = "The higher the value, the greater the defocus",
        update      = update_dof_lens_radius,
        min         = 0,
        max         = 10,
        default     = 0.5,
    )

    dof_focus_distance: FloatProperty(
        name        = "DOF focus distance",
        description = "The distance to the focus center",
        update      = update_dof_focus_distance,
        min         = 0,
        default     = 10,
    )




class HydraMaterialSettings(bpy.types.PropertyGroup):
    sample_as_light: BoolProperty(
        name        = "Multiple Importance Sample",
        description = "Use multiple importance sampling for this material, "
        "disabling may reduce overall noise for large "
        "objects that emit little light compared to other light sources",
        default     = True,
    )

    use_transparent_shadow: BoolProperty(
        name        = "Transparent Shadows",
        description = "Use transparent shadows for this material if it contains a Transparent BSDF, "
        "disabling will render faster but not give accurate shadows",
        default     = True,
    )

    thin_walled: BoolProperty(
        name        = "Thin walled",
        description = "Modeling of thin glass with geometry without volume",        
        default     = False,
    )

    cast_shadow: BoolProperty(
        name        = "Cast shadow",
        description = "Casting the shadow of an object with this material",        
        default     = True,
    )

    smooth_opacity: BoolProperty(
        name        = "Smooth opacity/alpha",
        description = "If disabled, the transparency texture has no gradients (speeds up rendering)",        
        default     = True,
    )

    fog_color: FloatVectorProperty(
        name        = "Distant color",
        description = "The color changes when passing through the volume",
        subtype     = 'COLOR',        
        default     = (1.0, 1.0, 1.0)
    )

    fog_multiplier: FloatProperty(
        name        = "Distant color multiplier",
        description = "A multiplier for changing the color",                
        default     = 1,
    )



class HydraLightSettings(bpy.types.PropertyGroup):
    def get_area(self):
        match self.hydra_light_type:
            case 'directional':
                return 1.0
            case 'point':
                if self.light_distribution == 'spot':
                    return 1.0
                else:
                    return 2.0 * math.pi

        match self.area_shape:
            case 'rect':
                return 4.0 * self.half_x * self.half_y
            case 'disk':
                return math.pi * self.radius * self.radius
            case 'sphere':
                return 4.0 * math.pi * self.radius * self.radius
            case 'cylinder':
                # return 2.0 * math.pi * self.radius * (self.height + self.radius)
                 return 2.0 * math.pi * self.radius * self.height # cylinder area without ends
            case _:
                return 1.0




    def update_light_type(self, context):
        '''Update functions to indicate settings in viewport.'''
        lamp       = self.id_data
        light_type = self.hydra_light_type

        match light_type:
            case 'directional':
                lamp.type = 'SUN'
            case 'area':
                lamp.type = 'AREA'
            case 'point':
                if self.light_distribution == 'spot':
                    lamp.type = 'SPOT'
                else:
                    lamp.type = 'POINT'

        lamp.energy = self.get_area() * self.multiplier * math.pi



    def update_distribution(self, context):
        lamp               = self.id_data
        light_distribution = self.light_distribution

        match self.hydra_light_type:
            case 'point':
                if   light_distribution    == 'spot' : lamp.type = 'SPOT'
                elif self.hydra_light_type == 'point': lamp.type = 'POINT'
            case 'ies':
                light_distribution = 'ies'
            case _:
                light_distribution = 'uniform'



    def update_area_shape(self, context):
        self.update_light_type(context)
        lamp = self.id_data
        match self.area_shape:
            case 'rect':
                lamp.shape  = 'RECTANGLE'
                lamp.size   = self.half_x * 2
                lamp.size_y = self.half_y * 2
            case 'disk':
                lamp.shape = 'DISK'
                lamp.size  = self.radius * 2
            case 'sphere':
                lamp.type = 'POINT'
                lamp.shadow_soft_size = self.radius
            case 'cylinder':
                lamp.shape  = 'RECTANGLE'
                lamp.size   = self.radius * 2
                lamp.size_y = self.height * 2


    def update_radius(self, context):
        lamp = self.id_data
        match self.area_shape:
            case 'disk':
                lamp.size = self.radius * 2
            case 'sphere':
                lamp.type = 'POINT'
                lamp.shadow_soft_size = self.radius
            case 'cylinder':
                lamp.size   = self.radius * 2

        lamp.energy = self.get_area() * self.multiplier * math.pi



    def update_size_x(self, context):
        lamp      = self.id_data
        lamp.size = self.half_x * 2
        lamp.energy = self.get_area() * self.multiplier * math.pi


    def update_size_y(self, context):
        lamp        = self.id_data
        lamp.size_y = self.half_y * 2
        lamp.energy = self.get_area() * self.multiplier * math.pi


    def update_spot_size(self, context):
        lamp           = self.id_data
        lamp.spot_size = self.spot_angle


    def update_spot_blend(self, context):
        lamp            = self.id_data
        lamp.spot_blend = self.spot_blend


    def update_cylinder_size(self, context):
        lamp        = self.id_data
        lamp.size_y = self.height
        lamp.energy = self.get_area() * self.multiplier * math.pi


    def update_multiplier(self, context):
        lamp        = self.id_data
        lamp.energy = self.get_area() * self.multiplier * math.pi


    def update_color(self, context):
        lamp        = self.id_data
        lamp.color  = self.color

    def update_direct_angle_radius(self, context):
        lamp        = self.id_data
        lamp.angle  = self.direct_angle_radius

    def update_direct_inner_radius(self, context):
        self.direct_outer_radius = max(self.direct_inner_radius * 1.01, self.direct_outer_radius)

    def update_direct_outer_radius(self, context):
        self.direct_inner_radius = min(self.direct_inner_radius, self.direct_outer_radius * 0.99)


    hydra_light_type: EnumProperty(
        name   = "Light Type",
        update = update_light_type,
        items  = [('area', 'area', 'Area Light'),
                  ('directional', 'directional', 'Directional Light'),
                  ('point', 'point', 'Point Light'),
                  #('portal', 'Portal', 'Portal Light'),
                 ],
        default = 'area'
    )


    area_shape: EnumProperty(
        name   = "Area Shape",
        update = update_area_shape,
        items  = [('rect', 'rect', 'Rectangle'),
                  ('disk', 'disk', 'Disk'),
                  ('sphere', 'sphere', 'Sphere'),
                  ('cylinder', 'cylinder', 'Cylinder'),],
        default = 'rect'
    )

    light_distribution: EnumProperty(
        name   = "Light Distribution",
        update = update_distribution,
        items  = [('uniform', 'uniform', 'Uniform'),
                  ('spot', 'spot', 'Spot'),
                  ('ies', 'IES', 'IES'),],
        default = 'uniform'
    )

    ies_path: StringProperty(
        name        = "IES",
        description = "IES file path",
        subtype     = 'FILE_PATH',
    )

    # Rectangle

    half_x: FloatProperty(
        name        = "Half Length",
        description = "Rectangle light half length",
        update      = update_size_x,
        min         = 0.000001,
        default     = 1.0,
        )

    half_y: FloatProperty(
        name        = "Half Width",
        description = "Rectangle light half width",
        update      = update_size_y,
        min         = 0.000001,
        default     = 1.0,
    )

    # Sphere, cylinder

    radius: FloatProperty(
        name        = "Radius",
        description = "Light radius",
        update      = update_radius,
        min         = 0.000001,
        default     = 1.0,
    )

    height: FloatProperty(
        name        = "Height",
        description = "Cylinder light height",
        update      = update_cylinder_size,
        min         = 0.000001,
        default     = 1.0,
    )

    angle: FloatProperty(
        name        = "Angle",
        description = "Cylinder light angle",
        subtype     = 'ANGLE',
        min         = 0.1, max = 360.0,
        default     = 360.0,
    )

    # Spot

    spot_blend: FloatProperty(
        name        = "The softness of the spotlight edge",
        description = "The softness of the spotlight edge",
        update      = update_spot_blend,
        min         = 0.0, max = 1.0,
        default     = 0.15,
    )

    spot_angle: FloatProperty(
        name        = "Angle of the spotlight beam",
        description = "Angle of the spotlight beam",
        subtype     = 'ANGLE',
        update      = update_spot_size,
        min         = 0.1, max = 3.141592,
        default     = 60.0,
    )

    # Direct

    direct_inner_radius: FloatProperty(
        name        = "Inner radius",
        description = "Directional light inner radius",
        update      = update_direct_inner_radius,
        min         = 0.0001,
        default     = 40.0,
    )

    direct_outer_radius: FloatProperty(
        name        = "Outer radius",
        description = "Directional light outer radius",
        update      = update_direct_outer_radius,
        min         = 0.0002,
        default     = 50.0,
    )

    direct_angle_radius: FloatProperty(
        name        = "Angular size",
        description = "Angular size of the light source",
        update      = update_direct_angle_radius,
        subtype     = 'ANGLE',
        min         = 0.0, max = math.pi,
        default     = 0,
    )

    # One thing works: either angular size, or shadow blur.
    # The angular size also changes the size of the source, it is more realistic and there is
    # support in the viewport, so we chose it.
    # direct_shadow_softness: FloatProperty(
    #     name        = "Shadow softness",
    #     description = "Shadow blur size",
    #     min         = 0,
    #     default     = 0,
    # )

    multiplier: FloatProperty(
        name        = "Multiplier",
        description = "Intensity multiplier",
        update      = update_multiplier,
        min         = 0.0,
        default     = 100.0,
    )

    color: FloatVectorProperty(
        name        = "Color",
        description = "Light color",
        subtype     = 'COLOR',
        update      = update_color,
        default     = (1.0, 1.0, 1.0)

    )


class HydraVisibilitySettings(bpy.types.PropertyGroup):

    camera: BoolProperty(
        name        = "Camera",
        description = "Object visibility for camera rays",
        default     = True,
    )
    diffuse: BoolProperty(
        name        = "Diffuse",
        description = "Object visibility for diffuse reflection rays",
        default     = True,
    )
    glossy: BoolProperty(
        name        = "Glossy",
        description = "Object visibility for glossy reflection rays",
        default     = True,
    )
    transmission: BoolProperty(
        name        = "Transmission",
        description = "Object visibility for transmission rays",
        default     = True,
    )
    shadow: BoolProperty(
        name        = "Shadow",
        description = "Object visibility for shadow rays",
        default     = True,
    )
    scatter: BoolProperty(
        name        = "Volume Scatter",
        description = "Object visibility for volume scatter rays",
        default     = True,
    )


class HydraMeshSettings(bpy.types.PropertyGroup):
    @classmethod
    def register(cls):
        hydra_mesh_settings  = "hydra Mesh Settings"
        bpy.types.Mesh.hydra_render = PointerProperty(
            name        = hydra_mesh_settings,
            description = hydra_mesh_settings,
            type        = cls,
        )
        bpy.types.Curve.hydra_render = PointerProperty(
            name        = hydra_mesh_settings,
            description = hydra_mesh_settings,
            type        = cls,
        )
        bpy.types.MetaBall.hydra_render = PointerProperty(
            name        = hydra_mesh_settings,
            description = hydra_mesh_settings,
            type        = cls,
        )

    @classmethod
    def unregister(cls):
        del bpy.types.Mesh.hydra_render
        del bpy.types.Curve.hydra_render
        del bpy.types.MetaBall.hydra_render


class HydraObjectSettings(bpy.types.PropertyGroup):

    use_motion_blur: BoolProperty(
        name        = "Use Motion Blur",
        description = "Use motion blur for this object",
        default     = True,
    )

    use_deform_motion: BoolProperty(
        name        = "Use Deformation Motion",
        description = "Use deformation motion blur for this object",
        default     = True,
    )

    motion_steps: IntProperty(
        name        = "Motion Steps",
        description = "Control accuracy of motion blur, more steps gives more memory usage (actual number of steps is 2^(steps - 1))",
        min         = 1, max = 7,
        default     = 1,
    )

    use_camera_cull: BoolProperty(
        name        = "Use Camera Cull",
        description = "Allow this object and its duplicators to be culled by camera space culling",
        default     = False,
    )

    use_distance_cull: BoolProperty(
        name        = "Use Distance Cull",
        description = "Allow this object and its duplicators to be culled by distance from camera",
        default     = False,
    )

    use_adaptive_subdivision: BoolProperty(
        name        = "Use Adaptive Subdivision",
        description = "Use adaptive render time subdivision",
        default     = False,
    )

    dicing_rate: FloatProperty(
        name        = "Dicing Scale",
        description = "Multiplier for scene dicing rate (located in the Subdivision panel)",
        min         = 0.1, max = 1000.0, soft_min = 0.5,
        default     = 1.0,
    )

    shadow_terminator_offset: FloatProperty(
        name        = "Shadow Terminator Offset",
        description = "Push the shadow terminator towards the light to hide artifacts on low poly geometry",
        min         = 0.0, max = 1.0,
        default     = 0.0,
    )

    is_shadow_catcher: BoolProperty(
        name        = "Shadow Catcher",
        description = "Only render shadows on this object, for compositing renders into real footage",
        default     = False,
    )

    is_holdout: BoolProperty(
        name        = "Holdout",
        description = "Render objects as a holdout or matte, creating a "
        "hole in the image with zero alpha, to fill out in "
        "compositing with real footage or another render",
        default = False,
    )


class HydraRenderLayerSettings(bpy.types.PropertyGroup):

    pass_debug_bvh_traversed_nodes: BoolProperty(
        name        = "Debug BVH Traversed Nodes",
        description = "Store Debug BVH Traversed Nodes pass",
        default     = False,
        update      = update_render_passes,
    )
    pass_debug_bvh_traversed_instances: BoolProperty(
        name        = "Debug BVH Traversed Instances",
        description = "Store Debug BVH Traversed Instances pass",
        default     = False,
        update      = update_render_passes,
    )
    pass_debug_bvh_intersections: BoolProperty(
        name        = "Debug BVH Intersections",
        description = "Store Debug BVH Intersections",
        default     = False,
        update      = update_render_passes,
    )
    pass_debug_ray_bounces: BoolProperty(
        name        = "Debug Ray Bounces",
        description = "Store Debug Ray Bounces pass",
        default     = False,
        update      = update_render_passes,
    )
    pass_debug_render_time: BoolProperty(
        name        = "Debug Render Time",
        description = "Render time in milliseconds per sample and pixel",
        default     = False,
        update      = update_render_passes,
    )
    pass_debug_sample_count: BoolProperty(
        name        = "Debug Sample Count",
        description = "Number of samples/camera rays per pixel",
        default     = False,
        update      = update_render_passes,
    )
    use_pass_volume_direct: BoolProperty(
        name        = "Volume Direct",
        description = "Deliver direct volumetric scattering pass",
        default     = False,
        update      = update_render_passes,
    )
    use_pass_volume_indirect: BoolProperty(
        name        = "Volume Indirect",
        description = "Deliver indirect volumetric scattering pass",
        default     = False,
        update      = update_render_passes,
    )

    use_denoising: BoolProperty(
        name        = "Use Denoising",
        description = "Denoise the rendered image",
        default     = True,
        update      = update_render_passes,
    )
    denoising_diffuse_direct: BoolProperty(
        name        = "Diffuse Direct",
        description = "Denoise the direct diffuse lighting",
        default     = True,
    )
    denoising_diffuse_indirect: BoolProperty(
        name        = "Diffuse Indirect",
        description = "Denoise the indirect diffuse lighting",
        default     = True,
    )
    denoising_glossy_direct: BoolProperty(
        name        = "Glossy Direct",
        description = "Denoise the direct glossy lighting",
        default     = True,
    )
    denoising_glossy_indirect: BoolProperty(
        name        = "Glossy Indirect",
        description = "Denoise the indirect glossy lighting",
        default     = True,
    )
    denoising_transmission_direct: BoolProperty(
        name        = "Transmission Direct",
        description = "Denoise the direct transmission lighting",
        default     = True,
    )
    denoising_transmission_indirect: BoolProperty(
        name        = "Transmission Indirect",
        description = "Denoise the indirect transmission lighting",
        default     = True,
    )
    denoising_strength: FloatProperty(
        name        = "Denoising Strength",
        description = "Controls neighbor pixel weighting for the denoising filter (lower values preserve more detail, but aren't as smooth)",
        min         = 0.0, max = 1.0,
        default     = 0.5,
    )
    denoising_feature_strength: FloatProperty(
        name        = "Denoising Feature Strength",
        description = "Controls removal of noisy image feature passes (lower values preserve more detail, but aren't as smooth)",
        min         = 0.0, max = 1.0,
        default     = 0.5,
    )
    denoising_radius: IntProperty(
        name        = "Denoising Radius",
        description = "Size of the image area that's used to denoise a pixel (higher values are smoother, but might lose detail and are slower)",
        min         = 1, max = 25,
        default     = 8,
        subtype     = "PIXEL",
    )
    denoising_relative_pca: BoolProperty(
        name        = "Relative Filter",
        description = "When removing pixels that don't carry information, use a relative threshold instead of an absolute one (can help to reduce artifacts, but might cause detail loss around edges)",
        default     = False,
    )
    denoising_store_passes: BoolProperty(
        name        = "Store Denoising Passes",
        description = "Store the denoising feature passes and the noisy image. The passes adapt to the denoiser selected for rendering",
        default     = False,
        update      = update_render_passes,
    )
    denoising_neighbor_frames: IntProperty(
        name        = "Neighbor Frames",
        description = "Number of neighboring frames to use for denoising animations (more frames produce smoother results at the cost of performance)",
        min         = 0, max = 7,
        default     = 0,
    )

    denoising_optix_input_passes: EnumProperty(
        name        = "Input Passes",
        description = "Passes used by the denoiser to distinguish noise from shader and geometry detail",
        items       = enum_denoising_input_passes,
        default     = 'RGB_ALBEDO',
    )

    denoising_openimagedenoise_input_passes: EnumProperty(
        name        = "Input Passes",
        description = "Passes used by the denoiser to distinguish noise from shader and geometry detail",
        items       = enum_denoising_input_passes,
        default     = 'RGB_ALBEDO_NORMAL',
    )

    use_pass_crypto_object: BoolProperty(
        name        = "Cryptomatte Object",
        description = "Render cryptomatte object pass, for isolating objects in compositing",
        default     = False,
        update      = update_render_passes,
    )
    use_pass_crypto_material: BoolProperty(
        name        = "Cryptomatte Material",
        description = "Render cryptomatte material pass, for isolating materials in compositing",
        default     = False,
        update      = update_render_passes,
    )
    use_pass_crypto_asset: BoolProperty(
        name        = "Cryptomatte Asset",
        description = "Render cryptomatte asset pass, for isolating groups of objects with the same parent",
        default     = False,
        update      = update_render_passes,
    )
    pass_crypto_depth: IntProperty(
        name        = "Cryptomatte Levels",
        description = "Sets how many unique objects can be distinguished per pixel",
        default     = 6, min = 2, max = 16, step = 2,
        update      = update_render_passes,
    )
    pass_crypto_accurate: BoolProperty(
        name        = "Cryptomatte Accurate",
        description = "Generate a more accurate Cryptomatte pass. CPU only, may render slower and use more memory",
        default     = True,
        update      = update_render_passes,
    )



class HydraDeviceSettings(bpy.types.PropertyGroup):
      id   : StringProperty(name="ID")
      name : StringProperty(name="Name")
      use  : BoolProperty(name="Use", default=True)
      type : EnumProperty(name="Type", items=enum_device_type, default='OPENCL')


class HydraPreferences(bpy.types.AddonPreferences):
    bl_idname = __package__

    install_path : bpy.props.StringProperty( name="Path to Hydra render", description='Path to Hydra render binary', subtype='DIR_PATH')

    def draw(self, context):
        self.layout.prop(self, "install_path")




class HydraView3DShadingSettings(bpy.types.PropertyGroup):
    render_pass: EnumProperty(
        name        = "Render Pass",
        description = "Render pass to show in the 3D Viewport",
        items       = enum_view3d_shading_render_pass,
        default     = 'COMBINED',
    )


classes = (
    HydraRenderSettings,
    HydraCameraSettings,
    HydraMaterialSettings,
    HydraLightSettings,
    HydraVisibilitySettings,
    HydraMeshSettings,
    HydraObjectSettings,
    HydraDeviceSettings,
    HydraPreferences,
    HydraRenderLayerSettings,
    HydraView3DShadingSettings)

def register():
    from bpy.utils import register_class

    for cls in classes:
        register_class(cls)


    bpy.types.Scene.hydra_render = PointerProperty(
        name        = "Hydra Renderer Settings",
        description = "Hydra renderer settings",
        type        = HydraRenderSettings)

    bpy.types.Camera.hydra_render = PointerProperty(
        name        = "Hydra Camera Settings",
        description = "Hydra camera settings",
        type        = HydraCameraSettings)

    bpy.types.Light.hydra_render = PointerProperty(
        name        = "Hydra Lamp Settings",
        description = "Hydra lamp settings",
        type        = HydraLightSettings)

    bpy.types.Material.hydra_render = PointerProperty(
        name        = "hydra Material Settings",
        description = "hydra material settings",
        type        = HydraMaterialSettings)

    bpy.types.Object.hydra_visibility = PointerProperty(
        name        = "Hydra Visibility Settings",
        description = "Hydra visibility settings",
        type        = HydraVisibilitySettings)

    bpy.types.World.hydra_visibility = PointerProperty(
        name        = "Hydra Visibility Settings",
        description = "Hydra visibility settings",
        type        = HydraVisibilitySettings)

    bpy.types.Object.hydra_render = PointerProperty(
        name        = "Hydra Object Settings",
        description = "Hydra object settings",
        type        = HydraObjectSettings)

    bpy.types.ViewLayer.hydra_render = PointerProperty(
        name        = "hydra ViewLayer Settings",
        description = "hydra ViewLayer Settings",
        type        = HydraRenderLayerSettings)

    bpy.types.View3DShading.hydra_render = PointerProperty(
        name        = "Hydra Settings",
        description = "Hydra settings",
        type        = HydraView3DShadingSettings)


def unregister():
    from bpy.utils import unregister_class

    for cls in reversed(classes):
        unregister_class(cls)
