from . import hydraPy as hy
import numpy as np






class TextureInfo:

    def __init__(self, ref, tile_u, tile_v):
        self.ref    = ref        
        self.tile_u = tile_u
        self.tile_v = tile_v

    def bind_texture(self, a_xml_node, a_linear_gamma = False):
        tex_node = hy.hrTextureBind(self.ref, a_xml_node, "texture")
        tex_node.force_attribute("matrix")
        sampler_matrix = np.array([ self.tile_u, 0, 0, 0,
                                    0, self.tile_v, 0, 0,
                                    0, 0, 1, 0,
                                    0, 0, 0, 1], dtype = np.float32)

        tex_node.force_attribute("addressing_mode_u").set_value("wrap")
        tex_node.force_attribute("addressing_mode_v").set_value("wrap")
        if a_linear_gamma:
            tex_node.append_attribute("input_gamma").set_value("1")
        hy.WriteMatrix4x4(tex_node, "matrix", sampler_matrix)
