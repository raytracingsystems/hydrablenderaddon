import os
import bpy
from bpy.types import Menu
from bl_ui import node_add_menu
from bpy.app.translations import (
    pgettext_iface as iface_,
)
from   bpy_extras.node_utils import find_node_input
from   bl_ui.utils           import PresetPanel
from   bpy.types             import Panel

# from bl_ui.properties_grease_pencil_common import GreasePencilSimplifyPanel


class HYDRA_PT_samplingPresets(PresetPanel, Panel):
    bl_label            = "Sampling Presets"
    preset_subdir       = "hydra/sampling"
    preset_operator     = "script.execute_preset"
    preset_add_operator = "render.hydra_sampling_preset_add"
    COMPAT_ENGINES      = {'HYDRA RENDERER'}


class HYDRA_PT_integratorPresets(PresetPanel, Panel):
    bl_label            = "Integrator Presets"
    preset_subdir       = "hydra/integrator"
    preset_operator     = "script.execute_preset"
    preset_add_operator = "render.hydra_integrator_preset_add"
    COMPAT_ENGINES      = {'HYDRA RENDERER'}


class HydraButtonsPanel:
    bl_space_type       = "PROPERTIES"
    bl_region_type      = "WINDOW"
    bl_context          = "render"
    COMPAT_ENGINES      = {'HYDRA RENDERER'}

    @classmethod
    def poll(cls, context):
        return context.engine in cls.COMPAT_ENGINES


class HYDRA_RENDER_PT_quality(HydraButtonsPanel, Panel):
    bl_label = "Quality"

    # def draw_header_preset(self, context):
    #     HydraRenderer_PT_samplingPresets.draw_panel_header(self.layout)

    def draw(self, context):
        layout                       = self.layout
        layout.use_property_split    = True
        layout.use_property_decorate = False

        cscene = context.scene.hydra_render

        layout.prop(cscene, "samples",            text="Samples per pixel", slider=True)

        row = layout.row(align=True)
        row.prop(cscene, "ray_bounces",           text="Bounces: ray, diffuse")
        row.prop(cscene, "diffuse_bounces",       text="")

        row = layout.row(align=True)
        row.prop(cscene, "use_limit_render_time", text="Limit time")        
        row.prop(cscene, "limit_render_time",     text="")



class HYDRA_RENDER_PT_engine(HydraButtonsPanel, Panel):
    bl_label = "Engine"
    # bl_options = {'DEFAULT_CLOSED'}

    # def draw_header_preset(self, context):
    #     HydraRenderer_PT_samplingPresets.draw_panel_header(self.layout)

    def draw(self, context):
        layout                       = self.layout
        layout.use_property_split    = True
        layout.use_property_decorate = False

        cscene = context.scene.hydra_render
        
        layout.prop(cscene, "version_engine")

        if context.scene.hydra_render.version_engine == "HYDRA ENGINE 2":
            layout.prop(cscene, "integrator_engine_2")
        elif context.scene.hydra_render.version_engine == "HYDRA ENGINE 3":
            layout.prop(cscene, "integrator_engine_3")

        if use_mmlt(context):
            layout.prop(cscene, "mmlt_estimate_time",        text="Estimate time", slider=True)

            row = layout.row(align=True)
            row.prop(cscene, "use_mmlt_median_filter",       text="Median filter")
            row.prop(cscene, "mmlt_median_filter_threshold", text="Threshold")            
        else:
            row = layout.row(align=True)
            row.prop(cscene, "use_caustics",                 text="Caustics")
            row.prop(cscene, "fast_background",              text="Fast background")

            row = layout.row(align=True)
            row.prop(cscene, "use_qmc_lights",               text="QMC lights")
            row.prop(cscene, "use_qmc_materials",            text="QMC materials")

            row = layout.row(align=True)
            row.prop(cscene, "use_bsdf_clamp",               text="Bsdf clamp")
            row.active = cscene.use_bsdf_clamp
            row.prop(cscene, "bsdf_clamp",                   text="")

            row = layout.row(align=True)
            row.prop(cscene, "auto_texture_resize",          text="Texture resize")

            if context.scene.hydra_render.version_engine == "HYDRA ENGINE 3":
                row.prop(cscene, "spectral_mode",                text="Spectral mode")


class HYDRA_RENDER_PT_override(HydraButtonsPanel, Panel):
    bl_label = "Override"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout                       = self.layout
        layout.use_property_split    = True
        layout.use_property_decorate = False

        cscene = context.scene.hydra_render

        layout.prop(cscene, "use_gray_materials", text="Gray materials")
        layout.prop(cscene, "envir_mult",         text="Envir. multiplier")




# class HydraRenderer_PT_samplingSubSamples(HydraButtonsPanel, Panel):
#     bl_label     = "Sub Samples"
#     bl_parent_id = "HYDRA_RENDER_PT_engine"

#     @classmethod
#     def poll(cls, context):
#         return use_mmlt(context)

#     def draw(self, context):
#         layout = self.layout
#         layout.use_property_split = True
#         layout.use_property_decorate = False

#         cscene = context.scene.hydra_render

#         col = layout.column(align=True)
#         col.prop(cscene, "diffuse_samples", text="Diffuse")
#         col.prop(cscene, "glossy_samples", text="Glossy")
#         col.prop(cscene, "transmission_samples", text="Transmission")
#         col.prop(cscene, "ao_samples", text="AO")

#         sub = col.row(align=True)
#         sub.active = use_sample_all_lights(context)
#         sub.prop(cscene, "mesh_light_samples", text="Mesh Light")
#         col.prop(cscene, "subsurface_samples", text="Subsurface")
#         col.prop(cscene, "volume_samples", text="Volume")



# class HydraRenderer_PT_denoising(HydraButtonsPanel, Panel):
#     bl_label = "Denoising"
#     bl_parent_id = "HYDRA_RENDER_PT_engine"
#     bl_options = {'DEFAULT_CLOSED'}

#     def draw(self, context):
#         layout = self.layout
#         layout.use_property_split = True
#         layout.use_property_decorate = False

#         scene = context.scene
#         cscene = scene.hydra_render

#         heading = layout.column(align=True, heading="Render")
#         row = heading.row(align=True)
#         row.prop(cscene, "use_denoising", text="")
#         sub = row.row()

#         sub.active = cscene.use_denoising
#         for view_layer in scene.view_layers:
#             if view_layer.hydra_render.denoising_store_passes:
#                 sub.active = True

#         sub.prop(cscene, "denoiser", text="")

#         heading = layout.column(align=False, heading="Viewport")
#         row = heading.row(align=True)
#         row.prop(cscene, "use_preview_denoising", text="")
#         sub = row.row()
#         sub.active = cscene.use_preview_denoising
#         sub.prop(cscene, "preview_denoiser", text="")

#         sub = heading.row(align=True)
#         sub.active = cscene.use_preview_denoising
#         sub.prop(cscene, "preview_denoising_start_sample", text="Start Sample")


class HYDRA_CAMERA_PT_camera(HydraButtonsPanel, Panel):
    bl_label   = "Hydra Settings"
    bl_context = "data"

    @classmethod
    def poll(cls, context):
        return context.camera and HydraButtonsPanel.poll(context)

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True

        camera  = context.camera
        hcamera = camera.hydra_render

        layout.prop(hcamera, "enable_dof", text="Enable DOF")
        layout.prop(hcamera, "dof_lens_radius", text="Lens Radius")
        layout.prop(hcamera, "dof_focus_distance", text="Focus Distance")


class HYDRA_LIGHT_PT_light(HydraButtonsPanel, Panel):
    bl_label   = "Light Settings"
    bl_context = "data"

    @classmethod
    def poll(cls, context):
        return context.light and HydraButtonsPanel.poll(context)

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True

        lamp  = context.light
        hlamp = lamp.hydra_render

        layout.prop(hlamp, "hydra_light_type", expand=True)

        if hlamp.hydra_light_type == 'area':
            layout.prop(hlamp, "area_shape", text="Shape", expand=True)
            if hlamp.area_shape == 'rect':
                layout.prop(hlamp, "half_x", text="Half Size X")
                layout.prop(hlamp, "half_y", text="Half Size Y")
            elif hlamp.area_shape in {'disk', 'sphere'}:
                layout.prop(hlamp, "radius", text="Radius")
            elif hlamp.area_shape == 'cylinder':
                layout.prop(hlamp, "radius", text="Radius")
                layout.prop(hlamp, "height", text="Height")
                layout.prop(hlamp, "angle",  text="Angle")

        if hlamp.hydra_light_type == 'directional':
            layout.prop(hlamp, "direct_inner_radius", text="Inner Radius")
            layout.prop(hlamp, "direct_outer_radius", text="Outer Radius")
            layout.prop(hlamp, "direct_angle_radius", text="Angular Size")
            # layout.prop(hlamp, "direct_shadow_softness", text="Shadow Blur") Does not work together with the angular size.


        layout.prop(hlamp, "color")
        layout.prop(hlamp, "multiplier", text="Multiplier")

        if hlamp.hydra_light_type != 'directional':
            layout.separator()
            layout.label(text="Distribution")
            layout.prop(hlamp, "light_distribution", text="Light Distribution", expand=True)

            if hlamp.light_distribution == 'ies':
                layout.prop(hlamp, "ies_path", text="IES")

            if hlamp.light_distribution == 'spot':
                layout.prop(hlamp, "spot_angle", text="Spot Size")
                layout.prop(hlamp, "spot_blend", text="Spot Softness", slider=True)




def panel_node_draw(layout, id_data, output_type, input_name):
    if not id_data.use_nodes:
        layout.operator("cycles.use_shading_nodes", icon='NODETREE')
        return False

    ntree = id_data.node_tree

    node = ntree.get_output_node('CYCLES')
    if node:
        input = find_node_input(node, input_name)
        if input:
            layout.template_node_view(ntree, node, input)
        else:
            layout.label(text="Incompatible output node")
    else:
        layout.label(text="No output node")

    return True

class HYDRA_WORLD_PT_surface(HydraButtonsPanel, Panel):
    bl_label   = "Surface"
    bl_context = "world"

    @classmethod
    def poll(cls, context):
        return context.world and HydraButtonsPanel.poll(context)

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True

        world = context.world

        if not panel_node_draw(layout, world, 'OUTPUT_WORLD', 'Surface'):
            layout.prop(world, "color")



# only show nodes working in object node trees
def object_shader_nodes_poll(context):
    snode = context.space_data
    return (snode.tree_type == 'ShaderNodeTree' and
            snode.shader_type == 'OBJECT')


class HYDRA_PT_context_material(HydraButtonsPanel, Panel):
    bl_label = ""
    bl_context = "material"
    bl_options = {'HIDE_HEADER'}

    @classmethod
    def poll(cls, context):
        if context.active_object and context.active_object.type == 'GPENCIL':
            return False
        else:
            return (context.material or context.object) and HydraButtonsPanel.poll(context)

    def draw(self, context):
        layout = self.layout

        mat   = context.material
        ob    = context.object
        slot  = context.material_slot
        space = context.space_data

        if ob:
            is_sortable = len(ob.material_slots) > 1
            rows = 3
            if (is_sortable):
                rows = 4

            row = layout.row()

            row.template_list("MATERIAL_UL_matslots", "", ob, "material_slots", ob, "active_material_index", rows=rows)

            col = row.column(align=True)
            col.operator("object.material_slot_add", icon='ADD', text="")
            col.operator("object.material_slot_remove", icon='REMOVE', text="")
            col.separator()
            col.menu("MATERIAL_MT_context_menu", icon='DOWNARROW_HLT', text="")

            if is_sortable:
                col.separator()

                col.operator("object.material_slot_move", icon='TRIA_UP', text="").direction = 'UP'
                col.operator("object.material_slot_move", icon='TRIA_DOWN', text="").direction = 'DOWN'

            if ob.mode == 'EDIT':
                row = layout.row(align=True)
                row.operator("object.material_slot_assign", text="Assign")
                row.operator("object.material_slot_select", text="Select")
                row.operator("object.material_slot_deselect", text="Deselect")

        row = layout.row()

        if ob:
            row.template_ID(ob, "active_material", new="material.new")

            if slot:
                icon_link = 'MESH_DATA' if slot.link == 'DATA' else 'OBJECT_DATA'
                row.prop(slot, "link", text="", icon=icon_link, icon_only=True)

        elif mat:
            layout.template_ID(space, "pin_id")
            layout.separator()


class HYDRA_MATERIAL_PT_preview(HydraButtonsPanel, Panel):
    bl_label   = "Preview"
    bl_context = "material"
    bl_options = {'DEFAULT_CLOSED'}

    @classmethod
    def poll(cls, context):
        mat = context.material
        return mat and (not mat.grease_pencil) and HydraButtonsPanel.poll(context)

    def draw(self, context):
        self.layout.template_preview(context.material)


class HYDRA_MATERIAL_PT_surface(HydraButtonsPanel, Panel):
    bl_label   = "Surface for Hydra renderer"
    bl_context = "material"

    @classmethod
    def poll(cls, context):
        mat = context.material
        return mat and (not mat.grease_pencil) and HydraButtonsPanel.poll(context) and mat.node_tree

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        obj    = context.object
        mat    = obj.active_material

        if mat and mat.node_tree:
            output_node = next((node for node in mat.node_tree.nodes if node.type == 'OUTPUT_MATERIAL'), None)
            if output_node and output_node.inputs['Surface'].is_linked:
                connected_node = output_node.inputs['Surface'].links[0].from_node
                cmat           = context.material
                hmat           = cmat.hydra_render

                if connected_node.type == 'BSDF_PRINCIPLED':
                    layout.prop(connected_node.inputs['Base Color'], "default_value", text="Base Color")
                    layout.prop(connected_node.inputs['Metallic'],   "default_value", text="Metallic")
                    layout.prop(connected_node.inputs['Roughness'],  "default_value", text="Roughness")
                    layout.prop(connected_node.inputs['IOR'],        "default_value", text="IOR")                    
                    if context.scene.hydra_render.version_engine == "HYDRA ENGINE 2":                            
                        layout.prop(hmat, "smooth_opacity", text="Smooth opacity")

                elif connected_node.type == 'BSDF_DIFFUSE':
                    layout.prop(connected_node.inputs['Color'],      "default_value", text="Color")
                    layout.prop(connected_node.inputs['Roughness'],  "default_value", text="Roughness") # oren-nayar

                elif connected_node.type == 'BSDF_GLASS':
                    layout.prop(connected_node.inputs['Color'],      "default_value", text="Color")
                    layout.prop(connected_node.inputs['IOR'],        "default_value", text="IOR")                    
                    if context.scene.hydra_render.version_engine == "HYDRA ENGINE 2":                    
                        layout.prop(connected_node.inputs['Roughness'],  "default_value", text="Roughness")
                        layout.prop(hmat, "fog_color",      text="Fog color")
                        layout.prop(hmat, "fog_multiplier", text="Fog multiplier")
                        layout.prop(hmat, "thin_walled",    text="Thin walled")
                
                if context.scene.hydra_render.version_engine == "HYDRA ENGINE 2":    
                    layout.prop(hmat, "cast_shadow", text="Cast shadow")



def get_device_type(context):
    return context.preferences.addons[__package__].preferences.compute_device_type


def use_cpu(context):
    cscene = context.scene.hydra_render
    return (get_device_type(context) == 'NONE' or cscene.device == 'CPU')


def use_opencl(context):
    cscene = context.scene.hydra_render
    return (get_device_type(context) == 'OPENCL' and cscene.device == 'GPU')


def use_mmlt(context):
    cscene = context.scene.hydra_render
    return (cscene.integrator_engine_2 == 'MMLT'or cscene.integrator_engine_3 == 'MMLT')


def use_sample_all_lights(context):
    cscene = context.scene.hydra_render
    return cscene.sample_all_lights_direct or cscene.sample_all_lights_indirect


def show_device_active(context):
    cscene = context.scene.hydra_render
    if cscene.device != 'GPU':
        return True
    return context.preferences.addons[__package__].preferences.has_active_device()


def draw_device(self, context):
    scene                        = context.scene
    layout                       = self.layout
    layout.use_property_split    = True
    layout.use_property_decorate = False

    if context.engine == 'HYDRA RENDERER':
        from . import engine
        cscene = scene.hydra_render

        col = layout.column()
        col.prop(cscene, "feature_set")

        # col = layout.column()
        # col.active = show_device_active(context)
        # col.prop(cscene, "device")



def draw_pause(self, context):
    layout = self.layout
    scene = context.scene

    if context.engine == "HYDRA RENDERER":
        view = context.space_data

        # if view.shading.type == 'RENDERED':
        #     cscene = scene.hydra_render
        #     layout.prop(cscene, "preview_pause", icon='PLAY' if cscene.preview_pause else 'PAUSE', text="")


def get_panels():
    exclude_panels = {
        'DATA_PT_area',
        'DATA_PT_camera_dof',
        'DATA_PT_falloff_curve',
        'DATA_PT_light',
        'DATA_PT_preview',
        'DATA_PT_spot',
        'MATERIAL_PT_context_material',
        'MATERIAL_PT_preview',
        'NODE_DATA_PT_light',
        'NODE_DATA_PT_spot',
        'OBJECT_PT_visibility',
        'VIEWLAYER_PT_filter',
        'VIEWLAYER_PT_layer_passes',
        'RENDER_PT_post_processing',
        'RENDER_PT_simplify',
        'RENDER_PT_freestyle',
        'RENDER_PT_grease_pencil',
        'RENDER_PT_gpencil',
    }

    panels = []
    for panel in bpy.types.Panel.__subclasses__():
        if hasattr(panel, 'COMPAT_ENGINES') and 'BLENDER_RENDER' in panel.COMPAT_ENGINES:
            if panel.__name__ not in exclude_panels:
                panels.append(panel)

    return panels

# Adapt properties editor panel to display in node editor. We have to
# copy the class rather than inherit due to the way bpy registration works.
def node_panel(cls):
    node_cls = type('NODE_' + cls.__name__, cls.__bases__, dict(cls.__dict__))

    node_cls.bl_space_type  = 'NODE_EDITOR'
    node_cls.bl_region_type = 'UI'
    node_cls.bl_category    = "Options"
    if hasattr(node_cls, 'bl_parent_id'):
        node_cls.bl_parent_id = 'NODE_' + node_cls.bl_parent_id

    return node_cls


classes = (
    HYDRA_PT_samplingPresets,
    HYDRA_PT_integratorPresets,
    HYDRA_RENDER_PT_quality,
    HYDRA_RENDER_PT_engine,
    HYDRA_RENDER_PT_override,
    # HydraRenderer_PT_denoising,
    HYDRA_CAMERA_PT_camera,
    HYDRA_LIGHT_PT_light,
    HYDRA_WORLD_PT_surface,
    HYDRA_PT_context_material,
    # HYDRA_MATERIAL_PT_preview,
    HYDRA_MATERIAL_PT_surface,
    node_panel(HYDRA_WORLD_PT_surface),
    node_panel(HYDRA_MATERIAL_PT_surface),
    # node_panel(hydra_MATERIAL_PT_settings),
    # node_panel(hydra_MATERIAL_PT_settings_surface),
    # node_panel(hydra_MATERIAL_PT_settings_volume),
    # node_panel(hydra_WORLD_PT_settings_surface),
    # node_panel(hydra_WORLD_PT_settings_volume),
    # node_panel(hydra_LIGHT_PT_light),
    # node_panel(hydra_LIGHT_PT_spot),
)



def register():
    from bpy.utils import register_class

    bpy.types.RENDER_PT_context.append(draw_device)
    bpy.types.VIEW3D_HT_header.append(draw_pause)

    for panel in get_panels():
        panel.COMPAT_ENGINES.add('HYDRA RENDERER')

    for cls in classes:
        register_class(cls)


def unregister():
    from bpy.utils import unregister_class

    bpy.types.RENDER_PT_context.remove(draw_device)
    bpy.types.VIEW3D_HT_header.remove(draw_pause)

    for panel in get_panels():
        if 'HYDRA RENDERER' in panel.COMPAT_ENGINES:
            panel.COMPAT_ENGINES.remove('HYDRA RENDERER')

    for cls in reversed(classes):
        unregister_class(cls)
