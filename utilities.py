import numpy as np
from . import hydraPy as hy
import os
import bpy
import time
from . import utilities as util

# filepath_bytes = os.fsencode(bpy.data.filepath)
# filepath_utf8 = filepath_bytes.decode('utf-8', "replace")
# bpy.context.object.name = filepath_utf8


def lerp(min, max, weight):
    return min + (max - min) * weight

def lerp_color(color1, color2, weight):
    r = color1[0] + (color2[0] - color1[0]) * weight
    g = color1[1] + (color2[1] - color1[1]) * weight
    b = color1[2] + (color2[2] - color1[2]) * weight
    return [r, g, b]


def clamp(val, a, b):
    r = max(a, val)
    return min(r, b)


def array_round_to_string(val, num_digits):
    return ' '.join(f"{round(x, num_digits):.{num_digits}f}" for x in val)


def round_to_string(val, num_digits):        
    return f"{round(val, num_digits):.{num_digits}f}"

def mult_color(color, val):
    r = color[0] * val
    g = color[1] * val
    b = color[2] * val
    return [r, g, b]


def get_real_path(path):
    return bpy.path.abspath(path)


def get_abspath(path, library=None, must_exist=False, must_be_existing_file=False, must_be_existing_dir=False):
    """ library: The library this path is from. """
    assert not (must_be_existing_file and must_be_existing_dir)

    abspath = bpy.path.abspath(path, library=library)

    if must_be_existing_file and not os.path.isfile(abspath):
        raise OSError('Not an existing file: "%s"' % abspath)

    if must_be_existing_dir and not os.path.isdir(abspath):
        raise OSError('Not an existing directory: "%s"' % abspath)

    if must_exist and not os.path.exists(abspath):
        raise OSError('Path does not exist: "%s"' % abspath)

    return abspath


def create_folders_if_notexist(new_folder):
    if not os.path.exists(new_folder):
        os.makedirs(new_folder)



def create_scenelib_folder(engine):
    if not os.path.exists(engine.scenepath):
        curr_folder = os.getcwd()
        new_folder  = os.path.join(curr_folder, "scenelib")
        create_folders_if_notexist(os.path.join(new_folder, "data"))
        report(engine, "WARNING", "Export path not set, using folder: " + new_folder)
        engine.scenepath = new_folder



def report(a_engine, a_type, a_message):
    ''' a_type is "INFO", "DEBUG", "ERROR", "WARNING" etc. '''
    mess = "Hydra renderer [" + a_type + "]: " + a_message

    if a_engine.debug:
        print(mess)
        a_engine.report({a_type}, mess)
    elif a_type != "DEBUG":
        a_engine.report({a_type}, mess)



def copy_image_to_framebuffer(a_engine, a_img_data):
    hy.hrRenderGetFrameBufferHDR4fNumPy(a_engine.render_ref, a_engine.render_w, a_engine.render_h, a_img_data, "color")
    # Here we write the pixel values to the RenderResult
    result      = a_engine.begin_result(0, 0, a_engine.render_w, a_engine.render_h)
    layer       = result.layers[0].passes["Combined"]
    layer.rect  = a_img_data.reshape(a_engine.img_size, 4) #very slow, 3.4 sec. for 1920x1080.
    a_engine.end_result(result)



def copy_alpha_to_framebuffer(a_engine, a_img_data):
    hy.hrRenderLockFrameBufferUpdate(a_engine.render_ref)

    for y in range(a_engine.render_h):
        g_buff_line = hy.hrRenderGetGBufferLine(a_engine.render_ref, a_engine.render_w, y)
        for x in range (a_engine.render_w):
            num_pix                   = y * a_engine.render_w + x
            a_img_data[num_pix*4 + 3] = g_buff_line[x].rgba[3]

    hy.hrRenderUnlockFrameBufferUpdate(a_engine.render_ref)

    # Here we write the pixel values to the RenderResult
    result     = a_engine.begin_result(0, 0, a_engine.render_w, a_engine.render_h)
    layer      = result.layers[0].passes["Combined"]
    layer.rect = a_img_data.reshape(a_engine.img_size, 4)
    a_engine.end_result(result)



def get_link(socket):
    """
    Returns the link if this socket is linked, None otherwise.
    All reroute nodes between this socket and the next non-reroute node are skipped.
    Muted nodes are ignored.
    """

    if not socket.is_linked:
        return None

    link = socket.links[0]

    while link.from_node.bl_idname == "NodeReroute" or link.from_node.mute:
        node = link.from_node

        if node.mute:
            if node.internal_links:
                # Only nodes defined in C can have internal_links in Blender
                links = node.internal_links[0].from_socket.links
                if links:
                    link = links[0]
                else:
                    return None
            else:
                if not link.from_socket.bl_idname.startswith("HydraCoreSocket") or not node.inputs:
                    return None

                # We can't define internal_links, so try to make up a link that makes sense.
                found_internal_link = False

                for input_socket in node.inputs:
                    if input_socket.links and link.from_socket.is_allowed_input(input_socket):
                        link = input_socket.links[0]
                        found_internal_link = True
                        break

                if not found_internal_link:
                    return None
        else:
            # Reroute node
            if node.inputs[0].is_linked:
                link = node.inputs[0].links[0]
            else:
                # If the left-most reroute has no input, it is like self.is_linked == False
                return None

    return link

